package buscompany.daoimpl;

import buscompany.dao.AuthenticationDao;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.Client;
import buscompany.model.Session;
import buscompany.model.User;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.UUID;

public class AuthenticationDaoImpl extends BaseDaoImpl implements AuthenticationDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationDao.class);

    @Override
    public String login(String login, String password, LocalDateTime dateTime) throws ServerException {
        LOGGER.info("AuthenticationDao: logIn user {}", login);
        String token = UUID.randomUUID().toString();
        try (SqlSession sqlSession = getSession()) {
            try {
                User user = getAuthenticationMapper(sqlSession).getUser(login, password);
                if (user == null) {
                    throw new ServerException(login + "/" + password, ServerErrorCode.INVALID_LOGIN_OR_PASSWORD, "LOGIN/PASSWORD");
                }
                if (getAuthenticationMapper(sqlSession).updateSession(new Session(user, token, dateTime)) == 0) {
                    getAuthenticationMapper(sqlSession).createSession(new Session(user, token, dateTime));
                }
            } catch (PersistenceException e) {
                LOGGER.info("AuthenticationDao: can`t logIn user {}, cause {}", login, e.getMessage());
                sqlSession.rollback();
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t logIn user {}, cause {}", login, e.getMessage());
                sqlSession.rollback();
                throw new ServerException(login + "/" + password, ServerErrorCode.INVALID_LOGIN_OR_PASSWORD, "LOGIN/PASSWORD");
            }
            sqlSession.commit();
            LOGGER.info("AuthenticationDao: logIn user {} successfully completed", login);
        }
        return token;
    }

    @Override
    public void logout(String token) {
        LOGGER.info("AuthenticationDao: logOut by session token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                getAuthenticationMapper(sqlSession).deleteSession(token);
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t logOut by session token {}, cause {}", token, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
            LOGGER.info("AuthenticationDao: logOut by session token {} successfully completed", token);
        }
    }

    @Override
    public void updateSessionDate(String token, LocalDateTime dateTime) {
        LOGGER.info("AuthenticationDao: updating session date by token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                getAuthenticationMapper(sqlSession).updateSessionDate(token, dateTime);
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t update session date by token {}, cause {}", token, e.getMessage());
                throw e;
            }
            sqlSession.rollback();
            LOGGER.info("AuthenticationDao: updating session date by token {} successfully completed", token);
        }
    }

    @Override
    public String getPassword(String token) {
        LOGGER.info("AuthenticationDao: getting password by session token {}", token);
        String password;
        try (SqlSession sqlSession = getSession()) {
            try {
                password = getAuthenticationMapper(sqlSession).getPassword(token);
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t get password by session token {}, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("AuthenticationDao: returns password by session token {} successfully completed", token);
        return password;
    }

    @Override
    public int checkOrderAccess(String token, int orderId) {
        int result;
        LOGGER.info("AuthenticationDao: getting password by session token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                result = getAuthenticationMapper(sqlSession).checkOrderAccess(token, orderId);
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t get password by session token {}, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("AuthenticationDao: returns password by session token {} successfully completed", token);
        return result;
    }

    @Override
    public Admin getAdminByToken(String token) {
        Admin admin;
        LOGGER.info("AuthenticationDao: getting admin by session token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                admin = getAdminMapper(sqlSession).getAdminByToken(token);
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t get admin by session token {}, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("AuthenticationDao: returns admin by session token {} successfully completed", token);
        return admin;
    }

    @Override
    public Client getClientByToken(String token) {
        Client client;
        LOGGER.info("AuthenticationDao: getting client by session token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                client = getClientMapper(sqlSession).getClientByToken(token);
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t get client by session token {}, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("AuthenticationDao: returns client by session token {} successfully completed", token);
        return client;
    }

    @Override
    public Session getSession(String token) {
        Session session;
        LOGGER.info("AuthenticationDao: getting session by token {}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                session = getAuthenticationMapper(sqlSession).getSession(token);
            } catch (RuntimeException e) {
                LOGGER.info("AuthenticationDao: can`t get session by token {}, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("AuthenticationDao: returns session by token {} successfully completed", token);
        return session;
    }
}
