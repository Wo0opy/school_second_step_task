package buscompany.daoimpl;

import buscompany.dao.TripDao;
import buscompany.model.Bus;
import buscompany.model.Trip;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TripDaoImpl extends BaseDaoImpl implements TripDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(TripDao.class);

    @Override
    public Bus getBusByName(String busName) {
        LOGGER.info("TripDao: getting the bus:{} from database", busName);
        Bus bus;
        try (SqlSession sqlSession = getSession()) {
            try {
                bus = getTripMapper(sqlSession).getBus(busName);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: getting bus:{} failed, cause {}", busName, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("TripDao: getting the bus:{} completed", busName);
        return bus;
    }

    @Override
    public void insertTrip(Trip trip) {
        LOGGER.info("TripDao: inserting trip:{}", trip);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTripMapper(sqlSession).insertTrip(trip);
                getTripMapper(sqlSession).insertTripDates(trip.getTripDates(), trip.getId(), trip.getBus().getPlaceCount());
                List<Integer> places = new ArrayList<>();
                Set<Integer> datesId = getTripMapper(sqlSession).getDatesId(trip.getId());
                for (int i = 1; i < trip.getBus().getPlaceCount() + 1; i++) {
                    places.add(i);
                }
                getTripMapper(sqlSession).insertTripPlaces(datesId, places);
                getTripMapper(sqlSession).insertSchedule(trip.getSchedule(), trip.getId());
                sqlSession.commit();
                LOGGER.info("TripDao: inserting trip completed. trip:{}", trip);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: inserting trip failed cause {}. trip:{}", e.getMessage(), trip);
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void approveTrip(int tripId) {
        LOGGER.info("TripDao: approving trip by id:{}", tripId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTripMapper(sqlSession).approveTrip(tripId);
                sqlSession.commit();
                LOGGER.info("TripDao: approving trip by id:{} completed", tripId);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: approving trip by id:{} failed, cause {}", tripId, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void removeTrip(int id) {
        LOGGER.info("TripDao: removing trip by id:{}", id);
        try (SqlSession sqlSession = getSession()) {
            try {
                getTripMapper(sqlSession).removeTrip(id);
                sqlSession.commit();
                LOGGER.info("TripDao: removing trip by id:{} completed", id);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: removing trip by id:{} failed, cause {}", id, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public Trip getTrip(int tripId) {
        LOGGER.info("TripDao: executing tripInfo request by tripId:{}", tripId);
        Trip trip;
        try (SqlSession sqlSession = getSession()) {
            try {
                trip = getTripMapper(sqlSession).getTrip(tripId);
                if (trip == null) {
                    return null;
                }
                trip.setTripDates(getTripMapper(sqlSession).getTripDates(trip));
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: tripInfo request by tripId:{} failed, cause {}", tripId, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("TripDao: tripInfo request by tripId:{} completed", tripId);
        return trip;
    }

    @Override
    public List<Trip> searchTrips(String fromStation, String toStation, String busName, boolean approved, LocalDate fromDate, LocalDate toDate) {
        LOGGER.info("TripDao: executing searchTripsForClient request");
        List<Trip> trips;
        try (SqlSession sqlSession = getSession()) {
            try {
                trips = getTripMapper(sqlSession).searchTrips(fromStation, toStation, approved, busName);
                trips.forEach(trip -> trip.setTripDates(getTripMapper(sqlSession).getTripDatesWithParams(fromDate, toDate, trip)));
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: searchTripsForClient request failed cause {}", e.getMessage());
                throw e;
            }
        }
        LOGGER.info("TripDao: searchTripsForClient request completed");
        return trips;
    }

    @Override
    public int updateTrip(Trip trip) {
        LOGGER.info("TripDao: executing updateTrip request. Request:{}", trip);
        int result;
        try (SqlSession sqlSession = getSession()) {
            try {
                result = getTripMapper(sqlSession).updateTrip(trip);
                getTripMapper(sqlSession).deleteDates(trip.getId());
                getTripMapper(sqlSession).insertTripDates(trip.getTripDates(), trip.getId(), trip.getBus().getPlaceCount());
                sqlSession.commit();
                LOGGER.info("TripDao: updateTrip request completed. Request:{}", trip);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: updateTrip request failed, cause {}. Request:{}", e.getMessage(), trip);
                sqlSession.rollback();
                throw e;
            }
        }
        return result;
    }

    @Override
    public List<Bus> getBuses() {
        LOGGER.info("TripDao: executing busList request");
        List<Bus> buses;
        try (SqlSession sqlSession = getSession()) {
            try {
                buses = getAdminMapper(sqlSession).getBuses();
            } catch (RuntimeException e) {
                LOGGER.info("TripDao:busList request failed cause {}", e.getMessage());
                throw e;
            }
        }
        return buses;
    }
}
