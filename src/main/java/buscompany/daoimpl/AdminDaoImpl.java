package buscompany.daoimpl;


import buscompany.dao.AdminDao;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.Session;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;

public class AdminDaoImpl extends BaseDaoImpl implements AdminDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminDao.class);

    @Override
    public Admin insert(Admin admin, String token, LocalDateTime localDateTime) throws ServerException {
        LOGGER.info("AdminDao: Trying to register admin: {}", admin.getLogin());
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).insertUser(admin);
                getAdminMapper(sqlSession).insertAdmin(admin.getId(), admin.getPosition());
                getAuthenticationMapper(sqlSession).createSession(new Session(admin, token, localDateTime));
                sqlSession.commit();
                LOGGER.info("AdminDao: {} registration completed", admin.getLogin());
            } catch (PersistenceException e) {
                sqlSession.rollback();
                if (SQLIntegrityConstraintViolationException.class.equals(e.getCause().getClass())) {
                    LOGGER.info("AdminDao: {} registration failed, cause {}", admin.getLogin(), ServerErrorCode.LOGIN_ALREADY_EXIST.getErrorString());
                    throw new ServerException(admin.getLogin(), ServerErrorCode.LOGIN_ALREADY_EXIST, "login");
                }
                throw e;
            } catch (RuntimeException e) {
                sqlSession.rollback();
                LOGGER.info("AdminDao: {} registration failed, cause {}", admin.getLogin(), e.getMessage());
            }
        }
        return admin;
    }

    @Override
    public Admin getAdmin(String token) {
        LOGGER.info("AdminDao: executing accountInfo request");
        Admin admin;
        try (SqlSession sqlSession = getSession()) {
            try {
                admin = getAdminMapper(sqlSession).getAdminByToken(token);
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("AdminDao: accountInfo request failed cause {}", e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
        return admin;
    }

    @Override
    public void update(Admin admin, String newPassword) {
        LOGGER.info("AdminDao: executing update user request {}", admin);
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).updateAdmin(admin, newPassword);
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("AdminDao: user update request by token: {}, failed cause {}", admin, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
        LOGGER.info("AdminDao: update user request by token:{} completed", admin);
    }

    @Override
    public void clearDataBase() {
        try (SqlSession sqlSession = getSession()) {
            try {
                getAdminMapper(sqlSession).clearUser();
                getAdminMapper(sqlSession).clearTrip();
                getAdminMapper(sqlSession).clearSchedule();
                sqlSession.commit();
            } catch (RuntimeException e) {
                sqlSession.rollback();
                throw e;
            }
        }
    }

    @Override
    public void remove(String token) throws ServerException {
        LOGGER.info("AdminDao: executing deleteAdmin request by token:{}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                if (getAdminMapper(sqlSession).getActiveAdminCount() == 1) {
                    LOGGER.info("AdminDao: deleteAdmin request by token:{} is failed, cause{} ", token, ServerErrorCode.LAST_ADMIN.getErrorString());
                    throw new ServerException(token, ServerErrorCode.LAST_ADMIN, "ADMIN");
                }
                getAdminMapper(sqlSession).removeAdmin(token);
                sqlSession.commit();
                LOGGER.info("AdminDao: deletion by token {} completed", token);
            } catch (RuntimeException | ServerException e) {
                sqlSession.rollback();
                LOGGER.info("AdminDao: deleteAdmin request by token:{} is failed, cause{} ", token, e.getMessage());
                throw e;
            }
        }
    }

}
