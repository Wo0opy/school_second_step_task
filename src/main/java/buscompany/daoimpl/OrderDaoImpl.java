package buscompany.daoimpl;

import buscompany.dao.OrderDao;
import buscompany.model.Client;
import buscompany.model.Order;
import buscompany.model.TripDate;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;

public class OrderDaoImpl extends BaseDaoImpl implements OrderDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderDao.class);

    @Override
    public Order createOrder(Order order, Client client) {
        LOGGER.info("OrderDao: executing createOrder request. {}", client);
        try (SqlSession sqlSession = getSession()) {
            try {
                int dateId = getOrderMapper(sqlSession).getDateId(order.getTripDate().getTrip().getId(), order.getTripDate().getDate());
                getOrderMapper(sqlSession).createOrder(order, client.getId(), dateId);
                getOrderMapper(sqlSession).insertPassengers(order.getPassengers(), order.getOrderId());
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: createOrder request failed. :{}. Cause {}", client, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
        LOGGER.info("OrderDao: createOrder request completed. :{}", client);
        return order;
    }

    @Override
    public List<Integer> getFreePlaces(int orderId) {
        LOGGER.info("OrderDao: executing getFreePlaces request");
        List<Integer> places;
        try (SqlSession sqlSession = getSession()) {
            try {
                places = getTripMapper(sqlSession).getFreePlaces(orderId);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: getFreePlaces request failed cause {}", e.getMessage());
                throw e;
            }
        }
        LOGGER.info("OrderDao: searchTripsForAdmin request completed");
        return places;
    }

    @Override
    public TripDate takePlace(String passportNumber, int placeNumber, int orderId) {
        LOGGER.info("OrderDao: executing takePlace request with params: passportNumber: {}, placeNumber: {}, orderId: {}", passportNumber, placeNumber, orderId);
        TripDate date;
        try (SqlSession sqlSession = getSession()) {
            try {
                int placeLimit = getOrderMapper(sqlSession).getPassengerPlaceLimit(passportNumber, orderId);
                List<Integer> passengerPlaces = getOrderMapper(sqlSession).getPassengerPlaces(passportNumber, orderId);
                int update = (getOrderMapper(sqlSession).takePlace(passportNumber, placeNumber, orderId));
                if (update == 0) {
                    return null;
                }
                if (placeLimit == passengerPlaces.size()) {
                    getOrderMapper(sqlSession).removePassengerPlace(passengerPlaces.get(0), placeNumber);
                }
                sqlSession.commit();
                date = getOrderMapper(sqlSession).getTripDateByOrderId(orderId);
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: takePlace request failed cause {}. Params: passportNumber: {}, placeNumber: {}, orderId: {}", e.getMessage(), passportNumber, placeNumber, orderId);
                sqlSession.rollback();
                throw e;
            }
        }
        LOGGER.info("OrderDao: takePlace request completed. Params: passportNumber: {}, placeNumber: {}, orderId: {}", passportNumber, placeNumber, orderId);
        return date;
    }

    @Override
    public List<Order> searchOrdersForAdmin(String fromStation, String toStation, String busName, LocalDate fromDate, LocalDate toDate, Integer clientId) {
        LOGGER.info("OrderDao: executing searchOrdersForAdmin request by clientId:{}", clientId);
        List<Order> orders;
        try (SqlSession sqlSession = getSession()) {
            try {
                orders = getOrderMapper(sqlSession).searchOrdersForAdmin(fromStation, toStation, busName, fromDate, toDate, clientId);
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: searchOrder searchOrdersForAdmin by clientId:{} failed, cause {}", clientId, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("OrderDao: searchOrdersForAdmin request by clientId:{} completed", clientId);
        return orders;

    }

    @Override
    public List<Order> searchOrdersForClient(String fromStation, String toStation, String busName, LocalDate fromDate, LocalDate toDate, String token) {
        LOGGER.info("OrderDao: executing searchOrdersForClient request by token:{}", token);
        List<Order> orders;
        try (SqlSession sqlSession = getSession()) {
            try {
                orders = getOrderMapper(sqlSession).searchOrdersForClient(fromStation, toStation, busName, fromDate, toDate, token);
            } catch (RuntimeException e) {
                LOGGER.info("TripDao: searchOrdersForClient request by token:{} failed, cause {}", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("OrderDao: searchOrdersForClient request by token:{} completed", token);
        return orders;
    }

    @Override
    public TripDate getTripDate(int tripId, LocalDate date) {
        LOGGER.info("OrderDao: executing tripDate request TripId:{}", tripId);
        TripDate tripDate;
        try (SqlSession sqlSession = getSession()) {
            try {
                tripDate = getOrderMapper(sqlSession).getTripDate(tripId, date);
                LOGGER.info("OrderDao: tripDate request completed. TripId:{}", tripId);
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: tripDate request failed, cause {}. TripId:{}", e.getMessage(), tripId);
                throw e;
            }
        }
        return tripDate;
    }

    @Override
    public int reserveSeats(TripDate tripDate, int placeCount) {
        LOGGER.info("OrderDao: trying to update tripDate. TripId:{}", tripDate.getTrip().getId());
        int success;
        try (SqlSession sqlSession = getSession()) {
            try {
                success = getOrderMapper(sqlSession).reserveSeats(tripDate, placeCount);
                sqlSession.commit();
                LOGGER.info("OrderDao: updating completed. TripId:{}", tripDate.getTrip().getId());
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: updating failed, cause {}. TripId:{}", e.getMessage(), tripDate.getTrip().getId());
                sqlSession.rollback();
                throw e;
            }
        }
        return success;
    }

    @Override
    public void removeOrder(int orderId) {
        LOGGER.info("OrderDao: executing removeOrder request. OrderId:{}", orderId);
        try (SqlSession sqlSession = getSession()) {
            try {
                getOrderMapper(sqlSession).cancelReservation(orderId);
                getOrderMapper(sqlSession).removeOrder(orderId);
                getOrderMapper(sqlSession).updateOrderPlaces(orderId);
                sqlSession.commit();
                LOGGER.info("OrderDao: removeOrder request completed. OrderId:{}", orderId);
            } catch (RuntimeException e) {
                LOGGER.info("OrderDao: removeOrder request failed, cause {}. OrderId:{}, orderId", e.getMessage(), orderId);
                sqlSession.rollback();
                throw e;
            }
        }
    }
}
