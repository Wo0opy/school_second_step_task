package buscompany.daoimpl;

import buscompany.mappers.*;
import buscompany.mybatis.utis.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

public class BaseDaoImpl {

    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    public AdminMapper getAdminMapper(SqlSession session) {
        return session.getMapper(AdminMapper.class);
    }

    public TripMapper getTripMapper(SqlSession session) {
        return session.getMapper(TripMapper.class);
    }

    public ClientMapper getClientMapper(SqlSession session) {
        return session.getMapper(ClientMapper.class);
    }

    public AuthenticationMapper getAuthenticationMapper(SqlSession session) {
        return session.getMapper(AuthenticationMapper.class);
    }

    public OrderMapper getOrderMapper(SqlSession session) {
        return session.getMapper(OrderMapper.class);
    }
}
