package buscompany.daoimpl;

import buscompany.dao.ClientDao;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.Client;
import buscompany.model.Session;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;

public class ClientDaoImpl extends BaseDaoImpl implements ClientDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientDao.class);

    @Override
    public Client insert(Client client, String token, LocalDateTime localDateTime) throws ServerException {
        LOGGER.info("ClientDao: Trying to register user: {}", client.getLogin());
        try (SqlSession sqlSession = getSession()) {
            try {
                getClientMapper(sqlSession).insertUser(client);
                getClientMapper(sqlSession).insertClient(client.getId(), client.getEmail(), client.getPhoneNumber());
                getAuthenticationMapper(sqlSession).createSession(new Session(client, token, localDateTime));
                sqlSession.commit();
            } catch (PersistenceException e) {
                sqlSession.rollback();
                if (SQLIntegrityConstraintViolationException.class.equals(e.getCause().getClass())) {
                    LOGGER.info("ClientDao:registration failed. {} is already exist.", client.getLogin());
                    throw new ServerException(client.getLogin(), ServerErrorCode.LOGIN_ALREADY_EXIST, "login");
                }
                throw e;
            } catch (RuntimeException e) {
                sqlSession.rollback();
                LOGGER.info("ClientDao: {} registration failed, cause {}", client.getLogin(), e.getMessage());
                throw e;
            }
        }
        LOGGER.info("ClientDao: {} registration successfully completed", client.getLogin());
        return client;
    }

    @Override
    public Client getClient(String token) {
        LOGGER.info("ClientDao: executing accountInfo request, by token:{}", token);
        Client client;
        try (SqlSession sqlSession = getSession()) {
            try {
                client = getClientMapper(sqlSession).getAccountInfo(token);
            } catch (RuntimeException e) {
                LOGGER.info("ClientDao: can`t get client info {}, cause {}.", token, e.getMessage());
                throw e;
            }
        }
        LOGGER.info("ClientDao: accountInfo request, by token:{}", token);
        return client;
    }

    @Override
    public List<Client> getAll() {
        LOGGER.info("ClientDao: executing clientList request");
        List<Client> clients;
        try (SqlSession sqlSession = getSession()) {
            try {
                clients = getClientMapper(sqlSession).getAll();
            } catch (RuntimeException e) {
                LOGGER.info("ClientDao: can`t get clientList, cause {}.", e.getMessage());
                throw e;
            }
        }
        LOGGER.info("ClientDao: clientList request completed");
        return clients;
    }

    @Override
    public void updateClient(Client client, String newPassword) {
        LOGGER.info("ClientDao: executing update user request :{}", client);
        try (SqlSession sqlSession = getSession()) {
            try {
                getClientMapper(sqlSession).updateClient(client, newPassword);
                sqlSession.commit();
            } catch (RuntimeException e) {
                LOGGER.info("ClientDao: update user request: {}, failed cause {}", client, e.getMessage());
                sqlSession.rollback();
                throw e;
            }
        }
        LOGGER.info("ClientDao: update user request:{} completed", client);
    }

    @Override
    public void delete(String token) {
        LOGGER.info("ClientDao: executing delete client request by token:{}", token);
        try (SqlSession sqlSession = getSession()) {
            try {
                getClientMapper(sqlSession).deleteClient(token);
                sqlSession.commit();
                LOGGER.info("ClientDao: deletion by token {} completed", token);
            } catch (RuntimeException e) {
                sqlSession.rollback();
                LOGGER.info("ClientDao: delete client request by token:{} is failed, cause{} ", token, e.getMessage());
                throw e;
            }
        }
    }
}
