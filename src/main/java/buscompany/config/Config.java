package buscompany.config;

import buscompany.daoimpl.*;
import buscompany.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    public ClientDaoImpl createClientDao() {
        return new ClientDaoImpl();
    }

    @Bean
    public AdminDaoImpl createAdminDao() {
        return new AdminDaoImpl();
    }

    @Bean
    public TripDaoImpl createTripDao() {
        return new TripDaoImpl();
    }

    @Bean
    public OrderDaoImpl createOrderDao() {
        return new OrderDaoImpl();
    }

    @Bean
    public AuthenticationDaoImpl createCompanyDao() {
        return new AuthenticationDaoImpl();
    }

    @Bean
    public AdminService createAdminService(AdminDaoImpl adminDao) {
        return new AdminService(adminDao);
    }

    @Bean
    public TripService createTripService(TripDaoImpl adminDao) {
        return new TripService(adminDao);
    }

    @Bean
    public ClientService createClientService(ClientDaoImpl clientDao) {
        return new ClientService(clientDao);
    }

    @Bean
    public AuthenticationService createAuthenticationService(AuthenticationDaoImpl authenticationDao) {
        return new AuthenticationService(authenticationDao);
    }

    @Bean
    public OrderService createOrderService(OrderDaoImpl orderDao) {
        return new OrderService(orderDao);
    }
}
