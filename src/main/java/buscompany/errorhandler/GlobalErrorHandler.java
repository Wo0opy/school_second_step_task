package buscompany.errorhandler;

import buscompany.dto.response.error.Error;
import buscompany.dto.response.error.ErrorResponse;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse validate(MethodArgumentNotValidException exc) {
        ErrorResponse errors = new ErrorResponse();
        exc.getBindingResult().getFieldErrors().forEach(error -> errors.getErrors().add
                (new Error(ServerErrorCode.INCORRECT_PARAMETER, error.getField(), error.getDefaultMessage())));
        return errors;
    }

    @ExceptionHandler({ServerException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse validationExceptionThrow(ServerException exc) {
        ErrorResponse errors = new ErrorResponse();
        errors.getErrors().add(new Error(exc));
        return errors;
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse unexpectedExceptionThrow(Exception exc) {
        ErrorResponse errors = new ErrorResponse();
        errors.getErrors().add(new Error(exc.getCause().toString()));
        return errors;
    }
}
