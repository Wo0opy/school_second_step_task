package buscompany.endpoint;

import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.AdminUpdateRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.ClientUpdateRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.AdminUpdateInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.ClientUpdateInfo;
import buscompany.dto.response.user.UserDTO;
import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.Client;
import buscompany.model.UserType;
import buscompany.mybatis.utis.MyBatisUtils;
import buscompany.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class UserEndpoint {
    private final AdminService adminService;
    private final TripService tripService;
    private final ClientService clientService;
    private final AuthenticationService authenticationService;
    private final OrderService orderService;

    @Autowired
    public UserEndpoint(AdminService adminService,
                        TripService tripService,
                        ClientService clientService,
                        AuthenticationService authenticationService,
                        OrderService orderService
    ) {
        this.adminService = adminService;
        this.tripService = tripService;
        this.clientService = clientService;
        this.authenticationService = authenticationService;
        this.orderService = orderService;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        MyBatisUtils.initSqlSessionFactory();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/admins")
    public AdminDTO insertAdmin(@Valid @RequestBody AdminRegistrationRequest request, HttpServletResponse response) throws ServerException {
        String token = UUID.randomUUID().toString();
        AdminDTO registrationResponse = adminService.insertAdmin(request, token);
        response.addCookie(new Cookie("JAVASESSIONID", token));
        return registrationResponse;
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/admins", method = RequestMethod.PUT)
    public AdminUpdateInfo updateAdmin(@Valid @RequestBody AdminUpdateRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletRequest httpRequest) throws ServerException {
        authenticationService.checkUpdateAccess(token, request.getOldPassword(), UserType.ADMIN);
        Admin admin = authenticationService.getAdminByToken(token);
        AdminUpdateInfo accountInfo = adminService.updateAdmin(admin, request);
        authenticationService.updateSessionDate(token);
        return accountInfo;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping("/clients")
    public ClientDTO insertClient(@Valid @RequestBody ClientRegistrationRequest request, HttpServletResponse response) throws ServerException {
        String token = UUID.randomUUID().toString();
        ClientDTO client = clientService.insert(request, token);
        response.addCookie(new Cookie("JAVASESSIONID", token));
        return client;
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/clients", method = RequestMethod.PUT)
    public ClientUpdateInfo updateClient(@Valid @RequestBody ClientUpdateRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletRequest httpRequest) throws ServerException {
        authenticationService.checkUpdateAccess(token, request.getOldPassword(), UserType.CLIENT);
        Client client = authenticationService.getClientByToken(token);
        ClientUpdateInfo clientUpdateInfo = clientService.updateClient(client, request);
        authenticationService.updateSessionDate(token);
        return clientUpdateInfo;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/sessions", method = RequestMethod.GET)
    public UserDTO getAccount(HttpServletRequest request, @CookieValue("JAVASESSIONID") String token) throws ServerException {
        UserDTO user;
        if (authenticationService.getUserTypeByToken(token).equals(UserType.ADMIN)) {
            user = adminService.getAdminAccountInfo(token);
            authenticationService.updateSessionDate(token);
            return user;
        }
        user = clientService.getClientAccountInfo(token);
        authenticationService.updateSessionDate(token);
        return user;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/clients", method = RequestMethod.GET)
    public List<Client> getClients(@CookieValue("JAVASESSIONID") String token, HttpServletRequest request) throws ServerException {
        authenticationService.checkToken(token, UserType.ADMIN);
        return clientService.getAll();
    }

    @DeleteMapping()
    @RequestMapping(value = "/accounts", method = RequestMethod.DELETE)
    public void removeUser(@CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(token);
        if (authenticationService.getUserTypeByToken(token).equals(UserType.ADMIN)) {
            adminService.delete(token);
        } else {
            clientService.delete(token);
        }
    }
}
