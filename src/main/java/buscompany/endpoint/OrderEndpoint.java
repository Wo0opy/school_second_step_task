package buscompany.endpoint;

import buscompany.dto.request.client.OrderRegistrationRequest;
import buscompany.dto.request.client.TakePlacesRequest;
import buscompany.dto.response.client.OrderResponse;
import buscompany.dto.response.client.TakePlacesResponse;
import buscompany.exception.ServerException;
import buscompany.model.Client;
import buscompany.model.UserType;
import buscompany.mybatis.utis.MyBatisUtils;
import buscompany.service.AuthenticationService;
import buscompany.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping("/api")
public class OrderEndpoint {
    private final AuthenticationService authenticationService;
    private final OrderService orderService;

    @Autowired
    public OrderEndpoint(AuthenticationService authenticationService,
                         OrderService orderService
    ) {
        this.authenticationService = authenticationService;
        this.orderService = orderService;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        MyBatisUtils.initSqlSessionFactory();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public List<OrderResponse> searchOrders(
            @RequestParam(value = "fromStation", required = false) String fromStation,
            @RequestParam(value = "toStation", required = false) String toStation,
            @RequestParam(value = "busName", required = false) String busName,
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam(value = "fromDate", required = false) LocalDate fromDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam(value = "toDate", required = false) LocalDate toDate,
            @RequestParam(value = "clientId", required = false) Integer clientId,
            @CookieValue("JAVASESSIONID") String token) throws ServerException {
        UserType type = authenticationService.getUserTypeByToken(token);
        List<OrderResponse> orders = orderService.searchOrders(fromStation, toStation, busName, fromDate, toDate, type, clientId, token);
        authenticationService.updateSessionDate(token);
        return orders;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/places/{orderId}", method = RequestMethod.GET)
    public List<Integer> getFreePlaces(@PathVariable("orderId") int orderId, @CookieValue("JAVASESSIONID") String token) throws ServerException {
        authenticationService.checkToken(token);
        List<Integer> freePlaces = orderService.getFreePlaces(orderId);
        authenticationService.updateSessionDate(token);
        return freePlaces;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public OrderResponse createOrder(@Valid @RequestBody OrderRegistrationRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(servletRequest.getCookies()[0].getValue(), UserType.CLIENT);
        Client client = authenticationService.getClientByToken(token);
        OrderResponse response = orderService.createOrder(request, client);
        authenticationService.updateSessionDate(token);
        return response;
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/orders/{orderId}", method = RequestMethod.DELETE)
    public void removeOrder(@PathVariable("orderId") int orderId, @CookieValue("JAVASESSIONID") String token) throws ServerException {
        authenticationService.checkOrderAccess(token, orderId);
        orderService.removeOrder(orderId);
        authenticationService.updateSessionDate(token);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/places", method = RequestMethod.POST)
    public TakePlacesResponse takePlace(@RequestBody TakePlacesRequest request, @CookieValue("JAVASESSIONID") String token) throws ServerException {
        authenticationService.checkOrderAccess(token, request.getOrderId());
        TakePlacesResponse response = orderService.takePlace(request);
        authenticationService.updateSessionDate(token);
        return response;
    }
}
