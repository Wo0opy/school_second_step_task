package buscompany.endpoint;

import buscompany.dto.request.login.LoginRequest;
import buscompany.exception.ServerException;
import buscompany.mybatis.utis.MyBatisUtils;
import buscompany.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.TimeZone;

@RestController
@RequestMapping("/api")
public class AuthenticationEndpoint {
    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationEndpoint(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        MyBatisUtils.initSqlSessionFactory();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/sessions", method = RequestMethod.POST)
    public void logIn(@Valid @RequestBody LoginRequest request, HttpServletResponse response) throws ServerException {
        response.addCookie(new Cookie("JAVASESSIONID", authenticationService.authenticate(request)));
    }

    @DeleteMapping()
    @RequestMapping(path = "/sessions", method = RequestMethod.DELETE)
    public void logout(HttpServletRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletResponse response) {
        authenticationService.logout(token);
        Cookie emptyCookie = new Cookie("JAVASESSIONID", null);
        emptyCookie.setMaxAge(0);
        response.addCookie(emptyCookie);
    }
}
