package buscompany.endpoint;

import buscompany.mybatis.utis.MyBatisUtils;
import buscompany.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.TimeZone;

@RestController
@RequestMapping("/api")
public class CompanyEndpoint {
    @Value("${user_idle_timeout}")
    private int user_idle_timeout;
    @Value("${min_password_length}")
    private int min_password_length;
    @Value("${max_name_length}")
    private int max_name_length;
    private final AdminService adminService;

    @Autowired
    public CompanyEndpoint(AdminService adminService) {
        this.adminService = adminService;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        MyBatisUtils.initSqlSessionFactory();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String getServerSettings() {
        return String.format("max_name_length = %s \nmin_password_length = %s", max_name_length, min_password_length);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/debug/clear", method = RequestMethod.POST)
    public void clearDataBase() {
        adminService.clearDataBase();
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/timeout", method = RequestMethod.GET)
    public int getUserIdleTimeout() {
        return user_idle_timeout;
    }
}
