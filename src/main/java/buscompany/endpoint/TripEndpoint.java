package buscompany.endpoint;

import buscompany.dto.bus.BusDTO;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.admin.TripUpdateRequest;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.tripinfo.TripInfo;
import buscompany.exception.ServerException;
import buscompany.model.UserType;
import buscompany.mybatis.utis.MyBatisUtils;
import buscompany.service.AuthenticationService;
import buscompany.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping("/api")
public class TripEndpoint {
    private final TripService tripService;
    private final AuthenticationService authenticationService;

    @Autowired
    public TripEndpoint(TripService tripService,
                        AuthenticationService authenticationService
    ) {
        this.tripService = tripService;
        this.authenticationService = authenticationService;
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
        MyBatisUtils.initSqlSessionFactory();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping("/buses")
    public List<BusDTO> getBuses(@CookieValue("JAVASESSIONID") String token, HttpServletRequest request) throws ServerException {
        authenticationService.checkToken(token, UserType.ADMIN);
        return tripService.getBuses();
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips", method = RequestMethod.POST)
    public TripAdminInfo insertTrip(@Valid @RequestBody TripRegistrationRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(token, UserType.ADMIN);
        return tripService.insertTrip(request);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips/{id}", method = RequestMethod.PUT)
    public TripAdminInfo updateTrip(@PathVariable("id") int id, @RequestBody TripUpdateRequest request, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(token, UserType.ADMIN);
        return tripService.updateTrip(request, id);
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips/{id}/approve", method = RequestMethod.PUT)
    public TripAdminInfo approveTrip(@PathVariable("id") int id, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(token);
        return tripService.approveTrip(id);
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips/{id}", method = RequestMethod.DELETE)
    public void removeTrip(@PathVariable("id") int id, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        authenticationService.checkToken(token, UserType.ADMIN);
        tripService.removeTrip(id);
        authenticationService.updateSessionDate(servletRequest.getCookies()[0].getValue());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips/{id}", method = RequestMethod.GET)
    public TripInfo getTripInfo(@PathVariable("id") int id, @CookieValue("JAVASESSIONID") String token, HttpServletRequest servletRequest) throws ServerException {
        TripInfo tripInfo;
        authenticationService.checkToken(token);
        tripInfo = tripService.getTripInfo(id, authenticationService.getUserTypeByToken(token));
        authenticationService.updateSessionDate(token);
        return tripInfo;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/trips", method = RequestMethod.GET)
    public List<? extends TripInfo> searchTrips(
            @RequestParam(value = "fromStation", required = false) String fromStation,
            @RequestParam(value = "toStation", required = false) String toStation,
            @RequestParam(value = "busName", required = false) String busName,
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam(value = "fromDate", required = false) LocalDate fromDate,
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            @RequestParam(value = "toDate", required = false) LocalDate toDate,
            @CookieValue("JAVASESSIONID") String token,
            HttpServletRequest servletRequest) throws ServerException {
        UserType type = authenticationService.getUserTypeByToken(token);
        List<? extends TripInfo> trips = tripService.searchTrips(fromStation, toStation, busName, fromDate, toDate, type);
        authenticationService.updateSessionDate(token);
        return trips;
    }
}
