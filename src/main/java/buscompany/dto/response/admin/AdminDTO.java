package buscompany.dto.response.admin;

import buscompany.dto.response.user.UserDTO;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
public class AdminDTO extends UserDTO {
    private String position;

    public AdminDTO(int id, String firstName, String lastName, String patronymic, String login, String type, String position) {
        super(id, firstName, lastName, patronymic, login, type);
        this.position = position;
    }
}
