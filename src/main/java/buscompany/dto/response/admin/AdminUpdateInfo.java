package buscompany.dto.response.admin;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AdminUpdateInfo {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String position;
    private String userType;

}
