package buscompany.dto.response.admin;


import buscompany.dto.response.tripinfo.TripInfo;
import buscompany.dto.schedule.ScheduleDTO;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TripAdminInfo extends TripInfo {
    private String busName;
    private String start;
    private int duration;
    private boolean approved;
    private ScheduleDTO schedule;
    private double price;
    private List<String> dates;

    public TripAdminInfo(int id,
                         String fromStation,
                         String toStation,
                         String busName,
                         String start,
                         int duration,
                         boolean approved,
                         ScheduleDTO schedule,
                         double price,
                         List<String> dates) {
        super(id, fromStation, toStation);
        this.busName = busName;
        this.start = start;
        this.duration = duration;
        this.approved = approved;
        this.schedule = schedule;
        this.price = price;
        this.dates = dates;
    }

    public TripAdminInfo(int id,
                         String fromStation,
                         String toStation,
                         String busName,
                         String start,
                         int duration,
                         boolean approved,
                         double price,
                         List<String> dates) {
        super(id, fromStation, toStation);
        this.busName = busName;
        this.start = start;
        this.duration = duration;
        this.approved = approved;
        this.price = price;
        this.dates = dates;
    }
}
