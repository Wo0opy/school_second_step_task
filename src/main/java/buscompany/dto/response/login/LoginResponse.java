package buscompany.dto.response.login;

import lombok.Getter;

@Getter
public class LoginResponse {
    private String login;
    private String password;

    public LoginResponse(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
