package buscompany.dto.response.client;

import buscompany.dto.response.user.UserDTO;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
public class ClientDTO extends UserDTO {
    private String email;
    private String phoneNumber;

    public ClientDTO(int id,
                     String firstName,
                     String lastName,
                     String patronymic,
                     String login,
                     String type,
                     String email,
                     String phoneNumber) {
        super(id, firstName, lastName, patronymic, login, type);
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
}
