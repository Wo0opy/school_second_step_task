package buscompany.dto.response.client;

import buscompany.dto.request.client.ClientUpdateRequest;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClientUpdateInfo {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String email;
    private String phoneNumber;
    private String userType;

    public ClientUpdateInfo(ClientUpdateRequest request) {
        this.firstName = request.getFirstName();
        this.lastName = request.getLastName();
        this.patronymic = request.getPatronymic();
        this.email = request.getEmail();
        this.phoneNumber = request.getEmail();
        this.userType = "CLIENT";
    }
}
