package buscompany.dto.response.client;

import buscompany.dto.passenger.PassengerDTO;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Getter
@Setter
public class OrderResponse {
    private int orderId;
    private int tripId;
    private String fromStation;
    private String toStation;
    private String busName;
    private String date;
    private String start;
    private int duration;
    private double price;
    private double totalPrice;
    private List<PassengerDTO> passengers;
}
