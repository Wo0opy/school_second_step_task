package buscompany.dto.response.client;

import buscompany.dto.request.client.TakePlacesRequest;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TakePlacesResponse {
    private int orderId;
    private String ticket;
    private String firstName;
    private String lastName;
    private String passportNumber;
    private int place;

    public TakePlacesResponse(String ticket, TakePlacesRequest request) {
        this.orderId = request.getOrderId();
        this.ticket = ticket;
        this.firstName = request.getFirstName();
        this.lastName = request.getLastName();
        this.passportNumber = request.getPassportNumber();
        this.place = request.getPlace();
    }
}
