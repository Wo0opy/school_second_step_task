package buscompany.dto.response.client;

import buscompany.dto.response.tripinfo.TripInfo;
import buscompany.dto.schedule.ScheduleDTO;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TripClientInfo extends TripInfo {
    private String busName;
    private String start;
    private int duration;
    private ScheduleDTO schedule;
    private double price;
    private List<String> dates;

    public TripClientInfo(int id,
                          String fromStation,
                          String toStation,
                          String busName,
                          String start,
                          int duration,
                          ScheduleDTO schedule,
                          double price,
                          List<String> dates) {
        super(id, fromStation, toStation);
        this.busName = busName;
        this.start = start;
        this.duration = duration;
        this.schedule = schedule;
        this.price = price;
        this.dates = dates;
    }

    public TripClientInfo(int id,
                          String fromStation,
                          String toStation,
                          String busName,
                          String start,
                          int duration,
                          double price,
                          List<String> dates) {
        super(id, fromStation, toStation);
        this.busName = busName;
        this.start = start;
        this.duration = duration;
        this.price = price;
        this.dates = dates;
    }
}
