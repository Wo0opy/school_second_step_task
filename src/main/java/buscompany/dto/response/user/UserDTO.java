package buscompany.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public abstract class UserDTO {
    private int id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String login;
    private String type;
}
