package buscompany.dto.response.error;

import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Getter
public class Error {
    private ServerErrorCode errorCode;
    private String field;
    private String message;

    public Error(ServerErrorCode errorCode, String field, String defaultMessage) {
        this.errorCode = errorCode;
        this.field = field;
        this.message = defaultMessage;
    }

    public Error(ServerException exc) {
        this.errorCode = exc.getErrorCode();
        this.field = exc.getField();
        this.message = exc.getMessage();
    }

    public Error(String message) {
        this.message = message;
    }
}
