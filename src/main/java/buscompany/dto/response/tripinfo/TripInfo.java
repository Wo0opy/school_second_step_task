package buscompany.dto.response.tripinfo;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public abstract class TripInfo {
    private int id;
    private String fromStation;
    private String toStation;

    public abstract List<String> getDates();
}
