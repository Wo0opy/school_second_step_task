package buscompany.dto.request.client;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TakePlacesRequest {
    private int orderId;
    private String firstName;
    private String lastName;
    private String passportNumber;
    private int place;
}
