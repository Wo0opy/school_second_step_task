package buscompany.dto.request.client;

import buscompany.dto.passenger.PassengerDTO;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderRegistrationRequest {
    private int tripId;
    private LocalDate date;
    private List<PassengerDTO> passengers;
}
