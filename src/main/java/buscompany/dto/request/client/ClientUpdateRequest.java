package buscompany.dto.request.client;

import buscompany.constraintvalidator.lengthvalidator.FieldLength;
import buscompany.constraintvalidator.lengthvalidator.PasswordLength;
import buscompany.constraintvalidator.phonevalidator.Phone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClientUpdateRequest {
    private static final String NAME_PATTERN = "^[а-яА-ЯёЁ][а-яА-ЯёЁ\\s -]*";
    @NotNull(message = "Укажите имя")
    @Pattern(regexp = NAME_PATTERN, message = "Имя может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым")
    @FieldLength(message = "Длина имени не более 50ти символов")
    private String firstName;
    @NotNull(message = "Укажите фамилию")
    @FieldLength(message = "Длина фамилии не более 50ти символов")
    @Pattern(regexp = NAME_PATTERN, message = "Фамилия может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым")
    private String lastName;
    @FieldLength(message = "Длина отчетсва не более 50ти символов")
    @Pattern(regexp = NAME_PATTERN, message = "Отчетство может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым")
    private String patronymic;
    @Email
    private String email;
    @NotNull(message = "Укажите номер телефона")
    @Phone
    private String phoneNumber;
    @PasswordLength(message = "Длина пароля от 8ми до 50ти символов")
    private String oldPassword;
    @NotBlank(message = "Введите пароль")
    @PasswordLength(message = "Длина пароля от 8ми до 50ти символов")
    private String newPassword;
}
