package buscompany.dto.request.login;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LoginRequest {
    @NotEmpty(message = "Введите имя вользователя")
    private String login;
    @NotEmpty(message = "Введите пароль")
    private String password;
}
