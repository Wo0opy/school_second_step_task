package buscompany.dto.request.admin;

import buscompany.constraintvalidator.lengthvalidator.FieldLength;
import buscompany.constraintvalidator.lengthvalidator.PasswordLength;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Getter
@AllArgsConstructor
public class AdminUpdateRequest {
    private static final String NAME_PATTERN = "^[а-яА-ЯёЁ][а-яА-ЯёЁ\\s -]*";
    @NotBlank(message = "Укажите имя.")
    @Pattern(regexp = NAME_PATTERN, message = "Имя может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым")
    @FieldLength(message = "Длина имени не более 50ти символов")
    private String firstName;
    @NotBlank(message = "Укажите фамилию")
    @FieldLength(message = "Длина фамилии не более 50ти символов")
    @Pattern(regexp = NAME_PATTERN, message = "Фамилия может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым")
    private String lastName;
    @Nullable
    @FieldLength(message = "Длина отчетсва не более 50ти символов")
    @Pattern(regexp = NAME_PATTERN, message = "Отчество может содержать только русские буквы, пробелы и знак “минус”")
    private String patronymic;
    @NotBlank(message = "Введите занимаемую должность")
    @FieldLength(message = "Длина должности не более 50ти символов")
    private String position;
    private String oldPassword;
    @NotBlank(message = "Введите пароль")
    @PasswordLength
    @Pattern(regexp = "[а-яА-ЯёЁa-zA-Z0-9]*", message = "Пароль может содержать только латинские и русские буквы и цифры и не может быть пустым")
    private String newPassword;

}
