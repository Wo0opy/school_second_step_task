package buscompany.dto.request.admin;

import buscompany.dto.schedule.ScheduleDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TripRegistrationRequest {
    private static final String DURATION_PATTERN = "^[0-9][0-9]*:\\d\\d$";
    @NotBlank(message = "Выберете марку автобуса")
    private String busName;
    @NotBlank(message = "Укажите станцию отправления")
    private String fromStation;
    @NotBlank(message = "Укажите станцию прибытия")
    private String toStation;
    @NotNull(message = "Укажите время отправления")
    private LocalTime start;
    @Pattern(regexp = DURATION_PATTERN, message = "Укажите время в формате ЧЧ:ММ")
    private String duration;
    @Min(value = 1, message = "Укажите цену билета")
    private double price;
    private ScheduleDTO schedule;
    private List<LocalDate> dates;

}
