package buscompany.dto.bus;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BusDTO {
    private String busName;
    private int placeCount;
}
