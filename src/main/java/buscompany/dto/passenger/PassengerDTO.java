package buscompany.dto.passenger;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PassengerDTO {
    private String firstName;
    private String lastName;
    private String passportNumber;
}
