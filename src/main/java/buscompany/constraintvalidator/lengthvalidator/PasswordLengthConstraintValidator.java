package buscompany.constraintvalidator.lengthvalidator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@PropertySource("classpath:/application.properties")
public class PasswordLengthConstraintValidator implements ConstraintValidator<PasswordLength, String> {
    @Value("${max_name_length}")
    private int max_name_length;
    @Value("${min_password_length}")
    private int min_password_length;

    @Override
    public void initialize(PasswordLength constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return s.length() >= min_password_length && s.length() <= max_name_length;
        }
        return true;
    }
}
