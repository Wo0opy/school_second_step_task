package buscompany.constraintvalidator.lengthvalidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = LengthConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldLength {
    String message() default "{Максимальная длина 50 символов}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
