package buscompany.constraintvalidator.lengthvalidator;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LengthConstraintValidator implements ConstraintValidator<FieldLength, String> {
    @Value("${max_name_length}")
    private int max_name_length;

    @Override
    public void initialize(FieldLength constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s != null) {
            return s.length() <= max_name_length;
        }
        return true;
    }
}
