package buscompany.constraintvalidator.lengthvalidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordLengthConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordLength {
    String message() default "Длина пароля от 8ти до 50ти символов";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
