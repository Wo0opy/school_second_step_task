package buscompany.constraintvalidator.phonevalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneConstraintValidator implements ConstraintValidator<Phone, String> {
    @Override
    public void initialize(Phone constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        String phoneNumber = s.replaceAll("-", "");
        if (phoneNumber.matches("^[8][0-9]{10}")) {
            return true;
        }
        return phoneNumber.matches("^[+][7][0-9]{10}");
    }
}
