package buscompany.model;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Schedule {
    private LocalDate fromDate;
    private LocalDate toDate;
    private String period;
}
