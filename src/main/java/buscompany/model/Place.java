package buscompany.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Place {
    private int placeNumber;
    private TripDate tripDate;

}
