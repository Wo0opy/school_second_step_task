package buscompany.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Passenger {
    private String firstName;
    private String lastName;
    private String passportNumber;
}
