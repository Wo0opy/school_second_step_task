package buscompany.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class TripDate {
    private Trip trip;
    private LocalDate date;

    public TripDate(Trip trip, LocalDate date) {
        this.trip = trip;
        this.date = date;
    }
}
