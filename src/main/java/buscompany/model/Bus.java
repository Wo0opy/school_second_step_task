package buscompany.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Bus {
    private String busName;
    private int placeCount;
}
