package buscompany.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Getter
@Setter
public class Order {
    private int orderId;
    private TripDate tripDate;
    private List<Passenger> passengers;

    public Order(TripDate tripDate) {
        this.tripDate = tripDate;
        this.passengers = new ArrayList<>();
    }
}
