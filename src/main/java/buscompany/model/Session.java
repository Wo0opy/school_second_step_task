package buscompany.model;

import lombok.*;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Session {
    private User user;
    private String token;
    private LocalDateTime date;
}
