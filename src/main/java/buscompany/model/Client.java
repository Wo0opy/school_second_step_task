package buscompany.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Client extends User {
    private String email;
    private String phoneNumber;

    public Client(String firstName, String lastName, String patronymic, String email, String phoneNumber, String login, String password, UserType type) {
        super(firstName, lastName, patronymic, login, password, type);
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Client(int id, String firstName, String lastName, String patronymic, String email, String phoneNumber, String login, String password, UserType type) {
        super(id, firstName, lastName, patronymic, login, password, type);
        this.email = email;
        this.phoneNumber = phoneNumber;
    }
}
