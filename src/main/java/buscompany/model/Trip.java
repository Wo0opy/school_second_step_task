package buscompany.model;

import lombok.*;

import java.time.LocalTime;
import java.util.List;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Trip {
    private int id;
    private Bus bus;
    private String fromStation;
    private String toStation;
    private LocalTime start;
    private int duration;
    private boolean approved;
    private Schedule schedule;
    private double price;
    @ToString.Exclude
    private List<TripDate> tripDates;

    public Trip(Bus bus,
                String fromStation,
                String toStation,
                LocalTime start,
                int duration,
                Schedule schedule,
                double price) {
        this.bus = bus;
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.start = start;
        this.duration = duration;
        this.approved = false;
        this.schedule = schedule;
        this.price = price;
    }

    public Trip(int id,
                Bus bus,
                String fromStation,
                String toStation,
                LocalTime start,
                int duration,
                Schedule schedule,
                double price) {
        this.id = id;
        this.bus = bus;
        this.fromStation = fromStation;
        this.toStation = toStation;
        this.start = start;
        this.duration = duration;
        this.schedule = schedule;
        this.price = price;
    }

}
