package buscompany.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class Admin extends User {
    private String position;

    public Admin(String firstName, String lastName, String patronymic, String position, String login, String password, UserType type) {
        super(firstName, lastName, patronymic, login, password, type);
        this.position = position;
    }

    public Admin(int id, String firstName, String lastName, String patronymic, String position, String login, String password, UserType type) {
        super(id, firstName, lastName, patronymic, login, password, type);
        this.position = position;
    }
}
