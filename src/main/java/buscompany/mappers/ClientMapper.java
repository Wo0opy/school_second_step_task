package buscompany.mappers;


import buscompany.model.Client;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface ClientMapper {
    @Insert("INSERT INTO user " +
            "(firstName, lastName, patronymic, login, password, type)" +
            "VALUES " +
            "(#{client.firstName}, " +
            "#{client.lastName}," +
            "#{client.patronymic}," +
            "#{client.login}," +
            "#{client.password}," +
            "#{client.type})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertUser(@Param("client") Client client);

    @Insert("INSERT INTO client " +
            "(userId, email, phoneNumber)" +
            "VALUES " +
            "(#{userId}," +
            "#{email}," +
            "#{phoneNumber})")
    void insertClient(@Param("userId") int userId, @Param("email") String email, @Param("phoneNumber") String phone);

    @Select("SELECT " +
            "user.id," +
            "user.firstName," +
            " user.lastName," +
            " user.patronymic," +
            " user.type " +
            "FROM user WHERE user.id=(SELECT userId FROM session WHERE token=#{token})")
    @Results({
            @Result(property = "email", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getEmailById", fetchType = FetchType.EAGER)),
            @Result(property = "phoneNumber", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getPhoneById", fetchType = FetchType.EAGER))
    })
    Client getAccountInfo(String token);

    @Select("SELECT " +
            "user.id," +
            " user.firstName," +
            " user.lastName," +
            " user.patronymic," +
            " user.type as userType" +
            " FROM user WHERE type='CLIENT'")
    @Results({
            @Result(property = "email", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getEmailById", fetchType = FetchType.EAGER)),
            @Result(property = "phoneNumber", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getPhoneById", fetchType = FetchType.EAGER))
    })
    List<Client> getAll();

    @Select("SELECT email FROM client WHERE userId=#{id}")
    String getEmailById();

    @Select("SELECT phoneNumber FROM client WHERE userId=#{id}")
    String getPhoneById();

    @Update("UPDATE user, client " +
            "set" +
            " user.firstName = #{client.firstName}," +
            " user.lastName = #{client.lastName}," +
            " user.patronymic = #{client.patronymic}," +
            " client.email = #{client.email}," +
            " client.phoneNumber = #{client.phoneNumber}," +
            " user.password = #{newPassword} " +
            "WHERE user.password = #{client.password}" +
            "AND user.id = #{client.id}")
    void updateClient(@Param("client") Client client, @Param("newPassword") String newPassword);

    @Update("UPDATE user SET deleted = true WHERE id = (SELECT userId from session WHERE token=#{token})")
    void deleteClient(@Param("token") String token);

    @Select("SELECT * " +
            "FROM user WHERE user.id=(SELECT userId FROM session WHERE token=#{token})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "lastName", column = "lastName"),
            @Result(property = "patronymic", column = "patronymic"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "email", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getEmailById", fetchType = FetchType.EAGER)),
            @Result(property = "phoneNumber", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.ClientMapper.getPhoneById", fetchType = FetchType.EAGER))
    })
    Client getClientByToken(String token);
}
