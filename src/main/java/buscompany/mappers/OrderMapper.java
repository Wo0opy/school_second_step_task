package buscompany.mappers;

import buscompany.model.Bus;
import buscompany.model.Order;
import buscompany.model.Passenger;
import buscompany.model.TripDate;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;

public interface OrderMapper {
    @Delete("DELETE FROM `order` WHERE id = #{orderId}")
    void removeOrder(int orderId);

    @Update("UPDATE place SET " +
            "passportNumber=null, " +
            "orderId = 0" +
            " WHERE orderId = #{orderId}")
    void updateOrderPlaces(@Param("orderId") int orderId);

    @Update("UPDATE place SET passportNumber=null," +
            "orderId = 0 " +
            "WHERE placeNumber = #{placeNumber} " +
            "AND tripId = (SELECT tripId FROM `order` WHERE order.id=#{orderId}) " +
            "AND passportNumber = (SELECT passportNumber FROM `order` WHERE id=#{orderId}) " +
            "AND tripDateId = (SELECT id FROM tripDate WHERE id = (SELECT tripId FROM `order` WHERE order.id=#{orderId}))")
    void removePassengerPlace(@Param("placeNumber") int place, @Param("orderId") int orderId);

    @Select("SELECT COUNT(*) FROM passenger WHERE passportNumber=#{passportNumber} AND orderId=#{orderId}")
    Integer getPassengerPlaceLimit(@Param("passportNumber") String passPortNumber, @Param("orderId") int orderId);

    @Select("SELECT placeNumber FROM place " +
            "WHERE passportNumber=#{passportNumber} " +
            "AND orderId = #{orderId} " +
            "AND tripDateId = (SELECT id FROM tripDate WHERE id = (SELECT tripId FROM `order` WHERE order.id=#{orderId}))")
    List<Integer> getPassengerPlaces(@Param("passportNumber") String passportNumber, @Param("orderId") int orderId);

    @Insert("INSERT INTO `order` (ownerId, tripDateId) VALUES " +
            "(#{ownerId}," +
            "#{tripDateId})")
    @Options(useGeneratedKeys = true, keyProperty = "order.orderId")
    void createOrder(@Param("order") Order order,
                     @Param("ownerId") int ownerId,
                     @Param("tripDateId") int dateId
    );

    @Insert({"<script>",
            "INSERT INTO passenger (firstName, lastName, passportNumber, orderId) VALUES",
            "<foreach item = 'item' collection = 'list' separator = ','>",
            "(#{item.firstName}, #{item.lastName}, #{item.passportNumber}, #{orderId})",
            "</foreach>",
            "</script>"})
    void insertPassengers(@Param("list") List<Passenger> passengers, @Param("orderId") int orderId);

    @Update("UPDATE place SET " +
            "passportNumber = #{passportNumber}," +
            "orderId=#{orderId} " +
            "WHERE placeNumber = #{placeNumber} " +
            "AND passportNumber IS NULL " +
            "AND tripDateId = (SELECT tripDateId FROM `order` WHERE id=#{orderId})")
    Integer takePlace(@Param("passportNumber") String passportNumber, @Param("placeNumber") int place, @Param("orderId") int orderId);

    @Select({"<script>",
            "SELECT `order`.id, tripDate.date, trip.fromStation, trip.toStation, trip.busName, trip.start, trip.duration, trip.price " +
                    "FROM `order` " +
                    "JOIN tripDate ON tripDate.id = `order`.tripDateId " +
                    "JOIN trip ON trip.id = tripDate.tripId ",
            "<where>",
            "<if test='fromStation != null'> AND fromStation = #{fromStation}",
            "</if>",
            "<if test='toStation != null'> AND toStation = #{toStation}",
            "</if>",
            "<if test='fromDate != null and toDate != null '> AND date BETWEEN #{fromDate} AND #{toDate}",
            "</if>",
            "<if test='toDate != null and fromDate==null'> AND date &lt;= #{toDate}",
            "</if>",
            "<if test='toDate == null and fromDate!=null'> AND date >= #{fromDate}",
            "</if>",
            "<if test='busName != null'> AND busName = #{busName}",
            "</if>",
            "<if test='clientId != null'> AND ownerId = #{clientId}",
            "</if>",
            "</where>" +
                    "</script>"})
    @Results({
            @Result(property = "orderId", column = "id"),
            @Result(property = "tripDate.trip.fromStation", column = "fromStation"),
            @Result(property = "tripDate.trip.toStation", column = "toStation"),
            @Result(property = "tripDate.trip.bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "tripDate.trip.id", column = "id", javaType = int.class,
                    one = @One(select = "buscompany.mappers.OrderMapper.getTripId", fetchType = FetchType.EAGER)),
            @Result(property = "tripDate.date", column = "date"),
            @Result(property = "tripDate.trip.start", column = "start"),
            @Result(property = "tripDate.trip.duration", column = "duration"),
            @Result(property = "tripDate.trip.price", column = "price"),
            @Result(property = "passengers", column = "id", javaType = List.class,
                    one = @One(select = "buscompany.mappers.OrderMapper.getOrderPassengers", fetchType = FetchType.EAGER))
    })
    List<Order> searchOrdersForAdmin(@Param("fromStation") String fromStation,
                                     @Param("toStation") String toStation,
                                     @Param("busName") String busName,
                                     @Param("fromDate") LocalDate fromDate,
                                     @Param("toDate") LocalDate toDate,
                                     @Param("clientId") Integer clientId);

    @Select("SELECT tripId FROM tripDate WHERE id=(SELECT tripDateId FROM `order` WHERE id=#{orderId})")
    int getTripId(@Param("orderId") int id);

    @Select("SELECT * FROM passenger WHERE orderId = #{orderId}")
    List<Passenger> getOrderPassengers(@Param("orderId") int id);

    @Select({"<script>",
            "SELECT `order`.id, tripDate.date, trip.fromStation, trip.toStation, trip.busName, trip.start, trip.duration, trip.price " +
                    "FROM `order` " +
                    "JOIN tripDate ON tripDate.id = `order`.tripDateId " +
                    "JOIN trip ON trip.id = tripDate.tripId ",
            "<if test='token != null'> AND ownerId = (SELECT userId FROM session WHERE token=#{token})",
            "</if>",
            "<if test='fromStation != null'> AND fromStation = #{fromStation}",
            "</if>",
            "<if test='toStation != null'> AND toStation = #{toStation}",
            "</if>",
            "<if test='fromDate != null and toDate != null '> AND date BETWEEN #{fromDate} AND #{toDate}",
            "</if>",
            "<if test='toDate != null and fromDate==null'> AND date &lt;= #{toDate}",
            "</if>",
            "<if test='toDate == null and fromDate!=null'> AND date >= #{fromDate}",
            "</if>",
            "<if test='busName != null'> AND busName = #{busName}",
            "</if>",
            "</script>"})
    @Results({
            @Result(property = "orderId", column = "id"),
            @Result(property = "tripDate.trip.fromStation", column = "fromStation"),
            @Result(property = "tripDate.trip.toStation", column = "toStation"),
            @Result(property = "tripDate.trip.bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "tripDate.trip.id", column = "id", javaType = int.class,
                    one = @One(select = "buscompany.mappers.OrderMapper.getTripId", fetchType = FetchType.EAGER)),
            @Result(property = "tripDate.date", column = "date"),
            @Result(property = "tripDate.trip.start", column = "start"),
            @Result(property = "tripDate.trip.duration", column = "duration"),
            @Result(property = "tripDate.trip.price", column = "price"),
            @Result(property = "passengers", column = "id", javaType = List.class,
                    one = @One(select = "buscompany.mappers.OrderMapper.getOrderPassengers", fetchType = FetchType.EAGER))
    })
    List<Order> searchOrdersForClient(@Param("fromStation") String fromStation,
                                      @Param("toStation") String toStation,
                                      @Param("busName") String busName,
                                      @Param("fromDate") LocalDate fromDate,
                                      @Param("toDate") LocalDate toDate,
                                      @Param("token") String token);

    @Select("SELECT id FROM tripDate WHERE tripId = #{tripId} AND date = #{date}")
    Integer getDateId(@Param("tripId") int tripId, @Param("date") LocalDate date);

    @Select("SELECT date, trip.id, trip.fromStation, trip.toStation, trip.busName, trip.start, trip.duration, trip.price FROM tripDate, trip" +
            " WHERE tripId = #{tripId}" +
            " AND date = #{date}" +
            " AND trip.id = #{tripId}")
    @Results({
            @Result(property = "date", column = "date"),
            @Result(property = "trip.id", column = "id"),
            @Result(property = "trip.fromStation", column = "fromStation"),
            @Result(property = "trip.toStation", column = "toStation"),
            @Result(property = "trip.bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "trip.start", column = "start"),
            @Result(property = "trip.duration", column = "duration"),
            @Result(property = "trip.price", column = "price"),
    })
    TripDate getTripDate(@Param("tripId") int tripId, @Param("date") LocalDate date);

    @Update("UPDATE tripDate" +
            " SET freePlaces = freePlaces - #{placeCount} " +
            "WHERE " +
            "date = #{tripDate.date} " +
            "AND tripId = #{tripDate.trip.id} " +
            "AND (freePlaces - #{placeCount}) >= 0"
    )
    int reserveSeats(@Param("tripDate") TripDate tripDate, @Param("placeCount") int placeCount);

    @Update("UPDATE tripDate" +
            " SET " +
            "freePlaces = freePlaces + (SELECT COUNT(*) FROM passenger WHERE passportNumber=#{passportNumber} AND orderId = #{orderId})" +
            "WHERE " +
            "tripDate.id = (SELECT tripDateId FROM `order` WHERE `order`.id = #{orderId}) "
    )
    void cancelReservation(int orderId);

    @Select("SELECT " +
            "date, " +
            "trip.id, " +
            "trip.fromStation, " +
            "trip.toStation, " +
            "trip.busName, " +
            "trip.start, " +
            "trip.duration, " +
            "trip.price " +
            "FROM tripDate JOIN trip ON trip.id = tripDate.tripId" +
            " WHERE tripDate.id = (SELECT tripDateId FROM `order` WHERE id = #{orderId})")
    @Results({
            @Result(property = "date", column = "date"),
            @Result(property = "trip.id", column = "id"),
            @Result(property = "trip.fromStation", column = "fromStation"),
            @Result(property = "trip.toStation", column = "toStation"),
            @Result(property = "trip.bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "trip.start", column = "start"),
            @Result(property = "trip.duration", column = "duration"),
            @Result(property = "trip.price", column = "price"),
    })
    TripDate getTripDateByOrderId(@Param("orderId") int orderId);

}
