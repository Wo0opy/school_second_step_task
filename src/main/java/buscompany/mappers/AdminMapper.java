package buscompany.mappers;

import buscompany.model.Admin;
import buscompany.model.Bus;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface AdminMapper {

    @Insert("INSERT INTO user " +
            "(firstName, lastName, patronymic, login, password, type)" +
            "VALUES " +
            "(#{admin.firstName}," +
            "#{admin.lastName}," +
            "#{admin.patronymic}," +
            "#{admin.login}," +
            "#{admin.password}," +
            "#{admin.type})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insertUser(@Param("admin") Admin admin);

    @Insert("INSERT INTO admin " +
            "(userId, position)" +
            "VALUES " +
            "(#{userId}," +
            "#{position})")
    void insertAdmin(@Param("userId") int userId, @Param("position") String position);

    @Select("SELECT * " +
            "FROM user " +
            "WHERE user.id = " +
            "(SELECT userId FROM session WHERE token=#{token})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "lastName", column = "lastName"),
            @Result(property = "patronymic", column = "patronymic"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "position", column = "id", javaType = Integer.class,
                    one = @One(select = "buscompany.mappers.AdminMapper.getAdminPosition", fetchType = FetchType.EAGER))
    })
    Admin getAdminByToken(String token);

    @Select("SELECT position FROM admin WHERE userId=#{id}")
    String getAdminPosition(@Param("id") int id);

    @Update("UPDATE user, admin " +
            "set" +
            " user.firstName = #{admin.firstName}," +
            " user.lastName = #{admin.lastName}," +
            " user.patronymic = #{admin.patronymic}," +
            " admin.position = #{admin.position}," +
            " user.password = #{newPassword} " +
            "WHERE user.password = #{admin.password}" +
            "AND user.id = #{admin.id}")
    void updateAdmin(@Param("admin") Admin admin, @Param("newPassword") String newPassword);

    @Select("SELECT * FROM bus")
    List<Bus> getBuses();

    @Delete("DELETE FROM user")
    void clearUser();

    @Delete("DELETE FROM trip")
    void clearTrip();

    @Delete("DELETE FROM schedule")
    void clearSchedule();

    @Select("SELECT COUNT(*) FROM user WHERE type = 'ADMIN'")
    int getActiveAdminCount();

    @Update("UPDATE user SET deleted = true WHERE id = (SELECT userId from session WHERE token=#{token})")
    void removeAdmin(@Param("token") String token);
}

