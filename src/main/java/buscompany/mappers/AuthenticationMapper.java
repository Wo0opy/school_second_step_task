package buscompany.mappers;

import buscompany.model.Session;
import buscompany.model.User;
import org.apache.ibatis.annotations.*;

import java.time.LocalDateTime;

public interface AuthenticationMapper {
    @Update("UPDATE session set token=#{session.token}, date=#{session.date}  WHERE userId=#{session.user.id}")
    Integer updateSession(@Param("session") Session session);

    @Update("UPDATE session set date=#{date}  WHERE token=#{token}")
    void updateSessionDate(String token, LocalDateTime date);

    @Insert("INSERT INTO session (userId, token, date) VALUES (#{session.user.id}, #{session.token}, #{session.date})")
    void createSession(@Param("session") Session session);

    @Delete("DELETE FROM session WHERE token=#{value}")
    void deleteSession(String value);

    @Select("SELECT password FROM user WHERE id = (SELECT userId FROM session WHERE token=#{token})")
    String getPassword(String token);

    @Select("SELECT COUNT(*) FROM `order` WHERE order.id = #{orderId} AND ownerId = (SELECT userId FROM session WHERE token=#{token})")
    int checkOrderAccess(@Param("token") String token, @Param("orderId") int orderId);

    @Select("SELECT id, firstName, lastName, patronymic, login, password, type " +
            "FROM user WHERE id = (SELECT id FROM user WHERE login=#{login} AND BINARY  password=#{password} AND deleted = false)")
    User getUser(@Param("login") String login, @Param("password") String password);

    @Select("SELECT " +
            "session.token, " +
            "session.date, " +
            "user.id, " +
            "user.firstName, " +
            "user.lastName, " +
            "user.patronymic, " +
            "user.login, " +
            "user.password, " +
            "user.type " +
            "FROM " +
            "session JOIN user ON user.id=session.userId " +
            "WHERE token = #{token}")
    @Results({
            @Result(property = "token", column = "token"),
            @Result(property = "date", column = "date"),
            @Result(property = "user.id", column = "id"),
            @Result(property = "user.firstName", column = "firstName"),
            @Result(property = "user.lastName", column = "lastName"),
            @Result(property = "user.patronymic", column = "patronymic"),
            @Result(property = "user.login", column = "login"),
            @Result(property = "user.password", column = "password"),
            @Result(property = "user.type", column = "type")
    })
    Session getSession(String token);
}
