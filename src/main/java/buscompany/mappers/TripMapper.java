package buscompany.mappers;

import buscompany.model.Bus;
import buscompany.model.Schedule;
import buscompany.model.Trip;
import buscompany.model.TripDate;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface TripMapper {
    @Insert("INSERT INTO trip " +
            "(fromStation, toStation, start, duration, price, busName)" +
            "VALUES " +
            "(#{trip.fromStation}," +
            "#{trip.toStation}," +
            "#{trip.start}," +
            "#{trip.duration}," +
            "#{trip.price}," +
            "(#{trip.bus.busName}))")
    @Options(useGeneratedKeys = true, keyProperty = "trip.id")
    void insertTrip(@Param("trip") Trip trip);

    @Select("SELECT busName, placeCount FROM bus WHERE busName=#{busName}")
    Bus getBus(String busName);

    @Update("UPDATE trip SET approved=true WHERE id=#{tripId} ")
    void approveTrip(int tripId);

    @Insert({"<script>",
            "INSERT INTO tripDate (date, tripId, freePlaces) VALUES",
            "<foreach item = 'item' collection = 'list' separator = ','>",
            "( #{item.date}, #{tripId}, #{freePlaces})",
            "</foreach>",
            "</script>"})
    void insertTripDates(@Param("list") List<TripDate> dates, @Param("tripId") int tripId, @Param("freePlaces") int freePlaces);

    @Insert({"<script>",
            "INSERT INTO place (placeNumber, tripDateId) VALUES",
            "<foreach item = 'placeNumber' collection = 'list' separator = ','>",
            "<foreach item = 'tripDateId' collection = 'set' separator = ','>",
            "( #{placeNumber}, #{tripDateId} )",
            "</foreach>",
            "</foreach>",
            "</script>"})
    void insertTripPlaces(@Param("set") Set<Integer> datesId, @Param("list") List<Integer> places);

    @Select("SELECT date FROM tripDate WHERE tripId=#{tripId}")
    Set<LocalDate> getDates(@Param("tripId") int tripId);

    @Delete("DELETE FROM tripDate WHERE tripId = #{tripId}")
    void deleteDates(@Param("tripId") int tripId);

    @Select("SELECT placeNumber FROM place " +
            "WHERE tripDateId = (SELECT tripDateId FROM `order` WHERE `order`.id = #{orderId} " +
            "AND passportNumber IS NULL)")
    List<Integer> getFreePlaces(@Param("orderId") int orderId);

    @Select({"<script>",
            "SELECT * FROM trip ",
            "<where>",
            "<if test='busName != null'> AND busName = #{busName}",
            "</if>",
            "<if test='toStation != null'> AND toStation = #{toStation}",
            "</if>",
            "<if test='fromStation != null'> AND fromStation = #{fromStation}",
            "</if>",
            "<if test='approved == true'> AND approved = #{approved}",
            "</if>",
            "</where>" +
                    "</script>"})
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "fromStation", column = "fromStation"),
            @Result(property = "toStation", column = "toStation"),
            @Result(property = "start", column = "start"),
            @Result(property = "duration", column = "duration"),
            @Result(property = "price", column = "price"),
            @Result(property = "bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "schedule", column = "id", javaType = Schedule.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getSchedule", fetchType = FetchType.EAGER))
    })
    List<Trip> searchTrips(@Param("fromStation") String fromStation,
                           @Param("toStation") String toStation,
                           @Param("approved") boolean approved,
                           @Param("busName") String busName);

    @Select({"<script>",
            "SELECT date FROM tripDate ",
            "<where>",
            "<if test='trip.id != null'> AND tripDate.tripId = #{trip.id}",
            "</if>",
            "<if test='fromDate != null and toDate != null '> AND date BETWEEN #{fromDate} AND #{toDate}",
            "</if>",
            "<if test='toDate != null and fromDate==null'> AND date &lt;= #{toDate}",
            "</if>",
            "<if test='toDate == null and fromDate!=null'> AND date >= #{fromDate}",
            "</if>",
            "</where>" +
                    "</script>"})
    List<TripDate> getTripDatesWithParams(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("trip") Trip trip);

    @Update("UPDATE trip, schedule " +
            "set " +
            "trip.fromStation = #{trip.fromStation}," +
            "trip.toStation = #{trip.toStation}," +
            "trip.start = #{trip.start}," +
            "trip.duration = #{trip.duration}," +
            "trip.price = #{trip.price}, " +
            "trip.busName = #{trip.bus.busName}, " +
            "trip.approved = false, " +
            "schedule.fromDate = #{trip.schedule.fromDate}, " +
            "schedule.toDate = #{trip.schedule.toDate}, " +
            "schedule.period = #{trip.schedule.period} " +
            "WHERE trip.id = #{trip.id} AND schedule.tripId = #{trip.id} AND approved=false")
    int updateTrip(@Param("trip") Trip trip);

    @Insert("INSERT INTO schedule (tripId, fromDate, toDate, period) " +
            "VALUES(" +
            "#{tripId}," +
            "#{schedule.fromDate}," +
            "#{schedule.toDate}," +
            "#{schedule.period})")
    void insertSchedule(@Param("schedule") Schedule schedule, @Param("tripId") int tripId);

    @Select("SELECT fromDate, toDate, period FROM schedule WHERE tripId = #{tripId}")
    Schedule getSchedule(@Param("tripId") int tripId);

    @Delete("DELETE FROM trip WHERE id = #{id}")
    void removeTrip(@Param("id") int id);

    @Select("SELECT id FROM tripDate WHERE tripId = #{id}")
    Set<Integer> getDatesId(int id);

    @Select("SELECT * FROM trip WHERE trip.id = #{tripId}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "fromStation", column = "fromStation"),
            @Result(property = "toStation", column = "toStation"),
            @Result(property = "start", column = "start"),
            @Result(property = "duration", column = "duration"),
            @Result(property = "approved", column = "approved"),
            @Result(property = "price", column = "price"),
            @Result(property = "bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "schedule", column = "id", javaType = Schedule.class,
                    many = @Many(select = "buscompany.mappers.TripMapper.getSchedule", fetchType = FetchType.EAGER))
    })
    Trip getTrip(@Param("tripId") int tripId);

    @Select("SELECT date, trip.id, trip.fromStation, trip.toStation, trip.busName, trip.start, trip.duration, trip.price FROM tripDate, trip" +
            " WHERE tripId = #{trip.id} " +
            " AND trip.id = #{trip.id}")
    @Results({
            @Result(property = "date", column = "date"),
            @Result(property = "trip.id", column = "id"),
            @Result(property = "trip.fromStation", column = "fromStation"),
            @Result(property = "trip.toStation", column = "toStation"),
            @Result(property = "trip.bus", column = "busName", javaType = Bus.class,
                    one = @One(select = "buscompany.mappers.TripMapper.getBus", fetchType = FetchType.EAGER)),
            @Result(property = "trip.start", column = "start"),
            @Result(property = "trip.duration", column = "duration"),
            @Result(property = "trip.price", column = "price"),
    })
    List<TripDate> getTripDates(@Param("trip") Trip trip);

}
