package buscompany.exception;

public enum ServerErrorCode {
    INVALID_TOKEN("Недействительный токен"),
    INVALID_LOGIN_OR_PASSWORD("Неправильный логин или пароль"),
    HAVE_NO_ACCESS("Нет прав доступа к ресурсу"),
    NOT_ENOUGH_FREE_PLACES("Недостаточно свободных мест"),
    SESSION_EXPIRES("Сессия недействительна"),
    WRONG_PASSWORD("Неверный пароль."),
    INCORRECT_PARAMETER,
    LOGIN_ALREADY_EXIST("Пользователь с ником %s уже существует"),
    WRONG_PASSENGER_DATA("Не корректно указаны данные пассажира."),
    PLACE_ALREADY_TAKEN("Место уже занято"),
    INCORRECT_SCHEDULE_OR_DATES("В запросе необходимо указать один из параметров - или расписание или даты отправления."),
    NO_TRIP_DATES_CALCULATED("Нет дат соответствующих расписанию рейса: %s"),
    LAST_ADMIN("Удаление аккаунта вожможно только после регистрации нового администратора"),
    UPDATE_FAIL("Рейс №%s устверждён или не существует"),
    WRONG_DATE_OR_ID("Неверно указана дата или номер рейса"),
    INCORRECT_SCHEDULE("Не корретно указан период рейса.");

    private String message;

    ServerErrorCode(String message) {
        this.message = message;
    }

    ServerErrorCode() {

    }

    public String getErrorString() {
        return message;
    }
}
