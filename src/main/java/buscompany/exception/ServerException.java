package buscompany.exception;

public class ServerException extends Exception {
    private ServerErrorCode errorCode;
    private String field;

    public ServerErrorCode getErrorCode() {
        return errorCode;
    }

    public ServerException(String message, ServerErrorCode errorCode, String field) {
        super(String.format(errorCode.getErrorString(), message));
        this.errorCode = errorCode;
        this.field = field;
    }

    public ServerException(ServerErrorCode errorCode, String field) {
        super(errorCode.getErrorString());
        this.errorCode = errorCode;
        this.field = field;
    }

    public String getField() {
        return field;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
