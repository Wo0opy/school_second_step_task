package buscompany.periodtype;

public enum DaysOfWeek {
    Sun(7),
    Mon(1),
    Tue(2),
    Wed(3),
    Thu(4),
    Fri(5),
    Sat(6);
    int day;

    DaysOfWeek(int day) {
        this.day = day;
    }

    public int getDay() {
        return day;
    }
}
