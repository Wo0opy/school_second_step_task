package buscompany.periodtype;

public enum DailyType {
    DAILY,
    EVEN,
    ODD
}
