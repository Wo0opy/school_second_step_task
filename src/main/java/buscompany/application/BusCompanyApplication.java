package buscompany.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "buscompany")
public class BusCompanyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BusCompanyApplication.class, args);
    }

}
