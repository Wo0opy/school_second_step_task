package buscompany.service;

import buscompany.dao.AuthenticationDao;
import buscompany.dto.request.login.LoginRequest;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.Client;
import buscompany.model.Session;
import buscompany.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;

public class AuthenticationService {
    private AuthenticationDao dao;
    @Value("${user_idle_timeout}")
    private int userIdleTimeout;

    @Autowired
    public AuthenticationService(AuthenticationDao dao) {
        this.dao = dao;
    }

    public String authenticate(LoginRequest request) throws ServerException {
        return dao.login(request.getLogin(), request.getPassword(), LocalDateTime.now().plusSeconds(userIdleTimeout));
    }

    public void updateSessionDate(String token) {
        dao.updateSessionDate(token, LocalDateTime.now().plusSeconds(userIdleTimeout));
    }

    public UserType getUserTypeByToken(String token) throws ServerException {
        Session session = dao.getSession(token);
        if (session == null) {
            throw new ServerException(token, ServerErrorCode.INVALID_TOKEN, "TOKEN");
        }
        if (session.getDate().isBefore(LocalDateTime.now())) {
            throw new ServerException(token, ServerErrorCode.SESSION_EXPIRES, "TOKEN");
        }
        return session.getUser().getType();
    }

    public void checkToken(String token, UserType userType) throws ServerException {
        Session session = dao.getSession(token);
        if (session == null) {
            throw new ServerException(token, ServerErrorCode.INVALID_TOKEN, "TOKEN");
        }
        if (session.getDate().isBefore(LocalDateTime.now())) {
            throw new ServerException(token, ServerErrorCode.SESSION_EXPIRES, "TOKEN");
        }
        if (!userType.equals(session.getUser().getType())) {
            throw new ServerException(ServerErrorCode.HAVE_NO_ACCESS, "TOKEN");
        }
    }

    public void checkToken(String token) throws ServerException {
        Session session = dao.getSession(token);
        if (session == null) {
            throw new ServerException(token, ServerErrorCode.INVALID_TOKEN, "TOKEN");
        }
        if (session.getDate().isBefore(LocalDateTime.now())) {
            throw new ServerException(token, ServerErrorCode.SESSION_EXPIRES, "TOKEN");
        }
    }

    public void logout(String value) {
        dao.logout(value);
    }

    public void checkUpdateAccess(String token, String oldPassword, UserType userType) throws ServerException {
        checkToken(token, userType);
        if (!dao.getPassword(token).equals(oldPassword)) {
            throw new ServerException(oldPassword, ServerErrorCode.WRONG_PASSWORD, "PASSWORD");
        }
    }

    public void checkOrderAccess(String token, int orderId) throws ServerException {
        checkToken(token);
        if (dao.checkOrderAccess(token, orderId) == 0) {
            throw new ServerException(ServerErrorCode.HAVE_NO_ACCESS, "TOKEN");
        }
    }

    public Admin getAdminByToken(String token) {
        return dao.getAdminByToken(token);
    }

    public Client getClientByToken(String token) {
        return dao.getClientByToken(token);
    }
}
