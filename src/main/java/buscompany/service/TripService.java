package buscompany.service;

import buscompany.dao.TripDao;
import buscompany.dto.bus.BusDTO;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.admin.TripUpdateRequest;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.TripClientInfo;
import buscompany.dto.response.tripinfo.TripInfo;
import buscompany.dto.schedule.ScheduleDTO;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.*;
import buscompany.periodtype.DailyType;
import buscompany.periodtype.DaysOfWeek;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class TripService {
    private TripDao dao;

    @Autowired
    public TripService(TripDao dao) {
        this.dao = dao;
    }

    public TripAdminInfo insertTrip(TripRegistrationRequest request) throws ServerException {
        Bus bus = dao.getBusByName(request.getBusName());
        Schedule schedule = parseSchedule(request.getSchedule());
        checkRequestSchedule(request.getDates(), schedule);
        String[] durationStrings = request.getDuration().split(":");
        int duration = (Integer.parseInt(durationStrings[0]) * 60) + Integer.parseInt(durationStrings[1]);
        Trip trip = new Trip(bus,
                request.getFromStation(),
                request.getToStation(),
                request.getStart(),
                duration,
                schedule,
                request.getPrice());
        if (request.getDates() != null) {
            List<TripDate> tripDates = new ArrayList<>();
            request.getDates().forEach(date -> tripDates.add(new TripDate(trip, date)));
            trip.setTripDates(tripDates);
            List<String> datesStrings = new ArrayList<>();
            request.getDates().forEach(date -> datesStrings.add(date.toString()));
            dao.insertTrip(trip);
            return new TripAdminInfo(trip.getId(),
                    trip.getFromStation(),
                    trip.getToStation(),
                    trip.getBus().getBusName(),
                    trip.getStart().toString(),
                    trip.getDuration(),
                    trip.isApproved(),
                    trip.getPrice(),
                    datesStrings);
        }
        List<LocalDate> dates = calculateDates(schedule);
        if (dates.size() == 0) {
            throw new ServerException(request.getSchedule().toString(), ServerErrorCode.NO_TRIP_DATES_CALCULATED, "SCHEDULE");
        }
        List<TripDate> tripDates = new ArrayList<>();
        dates.forEach(date -> tripDates.add(new TripDate(trip, date)));
        trip.setTripDates(tripDates);
        dao.insertTrip(trip);
        List<String> datesToString = new ArrayList<>();
        trip.getTripDates().forEach(date -> datesToString.add(date.getDate().toString()));
        TripAdminInfo info = new TripAdminInfo(trip.getId(),
                trip.getFromStation(),
                trip.getToStation(),
                trip.getBus().getBusName(),
                trip.getStart().toString(),
                trip.getDuration(),
                trip.isApproved(),
                trip.getPrice(),
                datesToString);
        if (trip.getSchedule() != null) {
            info.setSchedule(new ScheduleDTO(
                    trip.getSchedule().getFromDate().toString(),
                    trip.getSchedule().getToDate().toString(),
                    trip.getSchedule().getPeriod()));
        }
        return info;
    }

    public TripAdminInfo approveTrip(int tripId) {
        dao.approveTrip(tripId);
        return createAdminTripInfo(dao.getTrip(tripId));
    }

    private void checkRequestSchedule(List<LocalDate> dates, Schedule schedule) throws ServerException {
        if (dates != null && schedule != null) {
            throw new ServerException(ServerErrorCode.INCORRECT_SCHEDULE_OR_DATES, "SCHEDULE/DATES");
        }
        if (dates == null && schedule == null) {
            throw new ServerException(ServerErrorCode.INCORRECT_SCHEDULE_OR_DATES, "SCHEDULE/DATES");
        }
        if (dates != null && dates.size() == 0) {
            throw new ServerException(dates.toString(), ServerErrorCode.INCORRECT_SCHEDULE_OR_DATES, "DATES");
        }
    }

    private List<LocalDate> calculateDates(Schedule schedule) throws ServerException {
        String[] period = schedule.getPeriod().split(",");
        if (period.length == 1) {
            for (DailyType type : DailyType.values()) {
                if (type.name().equalsIgnoreCase(period[0])) {
                    return calculateDatesByDailyType(schedule, type);
                }
            }
        }
        try {
            int[] days = new int[period.length];
            for (int i = 0; i < period.length; i++) {
                days[i] = Integer.parseInt(period[i]);
            }
            return calculateDatesByDaysOfMonth(schedule, days);
        } catch (NumberFormatException e) {
            return calculateDatesByDaysOfWeek(schedule, period);
        }
    }

    private List<LocalDate> calculateDatesByDaysOfWeek(Schedule schedule, String[] period) throws ServerException {
        LocalDate date = schedule.getFromDate();
        List<LocalDate> dates = new ArrayList<>();
        for (String day : period) {
            while (date.isBefore(schedule.getToDate().plusDays(1))) {
                try {
                    if (DaysOfWeek.valueOf(day).getDay() == (date.getDayOfWeek().getValue())) {
                        dates.add(date);
                        date = date.plusWeeks(1);
                    } else {
                        date = date.plusDays(1);
                    }
                } catch (IllegalArgumentException e) {
                    throw new ServerException(schedule.toString(), ServerErrorCode.INCORRECT_SCHEDULE, "SCHEDULE");
                }
            }
            date = schedule.getFromDate();
        }
        return dates;
    }

    private List<LocalDate> calculateDatesByDaysOfMonth(Schedule schedule, int[] period) {
        LocalDate date = schedule.getFromDate();
        List<LocalDate> dates = new ArrayList<>();
        int months = (int) (date.until(schedule.getToDate(), ChronoUnit.MONTHS) + 1);
        for (int x = 0; x < months; x++) {
            for (Integer tripDay : period) {
                if (date.lengthOfMonth() >= tripDay) {
                    date = date.withDayOfMonth(tripDay);
                    dates.add(date);
                }
            }
            date = date.plusMonths(1);
        }
        return dates;
    }

    private List<LocalDate> calculateDatesByDailyType(Schedule schedule, DailyType type) {
        LocalDate date = schedule.getFromDate();
        List<LocalDate> dates = new ArrayList<>();
        long days = date.until(schedule.getToDate(), ChronoUnit.DAYS) + 1;
        switch (type) {
            case EVEN:
                while (days != 0) {
                    if ((date.getDayOfMonth() % 2) == 0) {
                        dates.add(date);
                        date = date.plusDays(1);
                    } else {
                        date = date.plusDays(1);
                    }
                    days--;
                }
                break;
            case ODD:
                while (days != 0) {
                    if ((date.getDayOfMonth() % 2) != 0) {
                        dates.add(date);
                        date = date.plusDays(1);
                    } else {
                        date = date.plusDays(1);
                    }
                    days--;
                }
                break;
            case DAILY:
                while (days != 0) {
                    dates.add(date);
                    date = date.plusDays(1);
                    days--;
                }
                break;
        }
        return dates;
    }

    public TripInfo getTripInfo(int id, UserType type) {
        return type.equals(UserType.ADMIN) ? createAdminTripInfo(dao.getTrip(id)) : createClientTripInfo(dao.getTrip(id));
    }

    public List<? extends TripInfo> searchTrips(String fromStation, String toStation, String busName, LocalDate fromDate, LocalDate toDate, UserType type) {
        List<Trip> trips;
        if (type.equals(UserType.ADMIN)) {
            List<TripAdminInfo> tripsInfo = new ArrayList<>();
            trips = dao.searchTrips(fromStation, toStation, busName, false, fromDate, toDate);
            trips.stream().filter(trip -> trip.getTripDates().size() != 0).forEach(trip -> tripsInfo.add(createAdminTripInfo(trip)));
            return tripsInfo;
        }
        List<TripClientInfo> tripsInfo = new ArrayList<>();
        trips = dao.searchTrips(fromStation, toStation, busName, true, fromDate, toDate);
        trips.stream().filter(trip -> trip.getTripDates().size() != 0).forEach(trip -> tripsInfo.add(createClientTripInfo(trip)));
        return tripsInfo;
    }

    private TripAdminInfo createAdminTripInfo(Trip trip) {
        if (trip == null) {
            return null;
        }
        List<String> dates = new ArrayList<>();
        trip.getTripDates().forEach(date -> dates.add(date.getDate().toString()));
        TripAdminInfo info = new TripAdminInfo(trip.getId(),
                trip.getFromStation(),
                trip.getToStation(),
                trip.getBus().getBusName(),
                trip.getStart().toString(),
                trip.getDuration(),
                trip.isApproved(),
                trip.getPrice(),
                dates);
        if (trip.getSchedule() != null) {
            info.setSchedule(new ScheduleDTO(
                    trip.getSchedule().getFromDate().toString(),
                    trip.getSchedule().getToDate().toString(),
                    trip.getSchedule().getPeriod()));
        }
        return info;
    }

    private TripClientInfo createClientTripInfo(Trip trip) {
        if (!trip.isApproved()) {
            return null;
        }
        List<String> dates = new ArrayList<>();
        trip.getTripDates().forEach(date -> dates.add(date.getDate().toString()));
        TripClientInfo info = new TripClientInfo(trip.getId(),
                trip.getFromStation(),
                trip.getToStation(),
                trip.getBus().getBusName(),
                trip.getStart().toString(),
                trip.getDuration(),
                trip.getPrice(),
                dates);
        if (trip.getSchedule() != null) {
            info.setSchedule(new ScheduleDTO(
                    trip.getSchedule().getFromDate().toString(),
                    trip.getSchedule().getToDate().toString(),
                    trip.getSchedule().getPeriod()));
        }
        return info;
    }

    public TripAdminInfo updateTrip(TripUpdateRequest request, int id) throws ServerException {
        Schedule schedule = parseSchedule(request.getSchedule());
        checkRequestSchedule(request.getDates(), schedule);
        Bus bus = dao.getBusByName(request.getBusName());
        String[] durationStrings = request.getDuration().split(":");
        int duration = (Integer.parseInt(durationStrings[0]) * 60) + Integer.parseInt(durationStrings[1]);
        Trip trip = new Trip(id, bus,
                request.getFromStation(),
                request.getToStation(),
                request.getStart(),
                duration,
                schedule,
                request.getPrice());
        if (request.getDates() != null) {
            List<TripDate> tripDates = new ArrayList<>();
            request.getDates().forEach(date -> tripDates.add(new TripDate(trip, date)));
            trip.setTripDates(tripDates);
            List<String> datesStrings = new ArrayList<>();
            request.getDates().forEach(date -> datesStrings.add(date.toString()));
            if (dao.updateTrip(trip) == 0) {
                throw new ServerException(String.valueOf(id), ServerErrorCode.UPDATE_FAIL, "ID");
            }
            TripAdminInfo info = new TripAdminInfo(trip.getId(),
                    trip.getFromStation(),
                    trip.getToStation(),
                    trip.getBus().getBusName(),
                    trip.getStart().toString(),
                    trip.getDuration(),
                    trip.isApproved(),
                    trip.getPrice(),
                    datesStrings);
            if (trip.getSchedule() != null) {
                info.setSchedule(new ScheduleDTO(
                        trip.getSchedule().getFromDate().toString(),
                        trip.getSchedule().getToDate().toString(),
                        trip.getSchedule().getPeriod()));
            }
            return info;
        }
        List<LocalDate> dates = calculateDates(schedule);
        if (dates.size() == 0) {
            throw new ServerException(ServerErrorCode.NO_TRIP_DATES_CALCULATED, "SCHEDULE");
        }
        List<TripDate> tripDates = new ArrayList<>();
        dates.forEach(date -> tripDates.add(new TripDate(trip, date)));
        trip.setTripDates(tripDates);
        if (dao.updateTrip(trip) == 0) {
            throw new ServerException(ServerErrorCode.UPDATE_FAIL, "ID");
        }
        List<String> datesToString = new ArrayList<>();
        dates.forEach(date -> datesToString.add(date.toString()));
        TripAdminInfo info = new TripAdminInfo(trip.getId(),
                trip.getFromStation(),
                trip.getToStation(),
                trip.getBus().getBusName(),
                trip.getStart().toString(),
                trip.getDuration(),
                trip.isApproved(),
                trip.getPrice(),
                datesToString);
        if (trip.getSchedule() != null) {
            info.setSchedule(new ScheduleDTO(
                    trip.getSchedule().getFromDate().toString(),
                    trip.getSchedule().getToDate().toString(),
                    trip.getSchedule().getPeriod()));
        }
        return info;
    }

    public void removeTrip(int id) {
        dao.removeTrip(id);
    }

    public List<BusDTO> getBuses() {
        ArrayList<BusDTO> busList = new ArrayList<>();
        dao.getBuses().forEach(bus -> busList.add(new BusDTO(bus.getBusName(), bus.getPlaceCount())));
        return busList;
    }

    private Schedule parseSchedule(ScheduleDTO scheduleDTO) throws ServerException {
        if (scheduleDTO == null) {
            return null;
        }
        Schedule schedule;
        try {
            schedule = new Schedule(LocalDate.parse(scheduleDTO.getFromDate()),
                    LocalDate.parse(scheduleDTO.getToDate()),
                    scheduleDTO.getPeriod());
        } catch (DateTimeParseException e) {
            throw new ServerException(ServerErrorCode.INCORRECT_SCHEDULE, "SCHEDULE");
        }
        return schedule;
    }
}
