package buscompany.service;

import buscompany.dao.OrderDao;
import buscompany.dto.passenger.PassengerDTO;
import buscompany.dto.request.client.OrderRegistrationRequest;
import buscompany.dto.request.client.TakePlacesRequest;
import buscompany.dto.response.client.OrderResponse;
import buscompany.dto.response.client.TakePlacesResponse;
import buscompany.exception.ServerErrorCode;
import buscompany.exception.ServerException;
import buscompany.model.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderService {
    private OrderDao dao;

    @Autowired
    public OrderService(OrderDao dao) {
        this.dao = dao;
    }

    public OrderResponse createOrder(OrderRegistrationRequest request, Client client) throws ServerException {
        TripDate tripDate = dao.getTripDate(request.getTripId(), request.getDate());
        if (tripDate == null) {
            throw new ServerException(ServerErrorCode.WRONG_DATE_OR_ID, "DATE/ID");
        }
        Order order = new Order(tripDate);
        for (PassengerDTO passenger : request.getPassengers()) {
            if (passenger.getFirstName() == null || passenger.getLastName() == null || passenger.getPassportNumber().length() != 10) {
                throw new ServerException(ServerErrorCode.WRONG_PASSENGER_DATA, "PASSENGERS");
            }
            order.getPassengers().add(new Passenger(passenger.getFirstName(), passenger.getLastName(), passenger.getPassportNumber()));
        }
        int update = dao.reserveSeats(tripDate, request.getPassengers().size());
        if (update == 0) {
            throw new ServerException(ServerErrorCode.NOT_ENOUGH_FREE_PLACES, "PASSENGERS");
        }
        dao.createOrder(order, client);
        return new OrderResponse(order.getOrderId(),
                order.getTripDate().getTrip().getId(),
                order.getTripDate().getTrip().getFromStation(),
                order.getTripDate().getTrip().getToStation(),
                order.getTripDate().getTrip().getBus().getBusName(),
                order.getTripDate().getDate().toString(),
                order.getTripDate().getTrip().getStart().toString(),
                order.getTripDate().getTrip().getDuration(),
                order.getTripDate().getTrip().getPrice(),
                order.getTripDate().getTrip().getPrice() * order.getPassengers().size(),
                request.getPassengers()
        );
    }

    public List<Integer> getFreePlaces(int orderId) {
        return dao.getFreePlaces(orderId);
    }

    public TakePlacesResponse takePlace(TakePlacesRequest request) throws ServerException {
        TripDate date = dao.takePlace(request.getPassportNumber(), request.getPlace(), request.getOrderId());
        if (date == null) {
            throw new ServerException(ServerErrorCode.PLACE_ALREADY_TAKEN, "PLACE");
        }
        Place place = new Place(request.getPlace(), date);
        return new TakePlacesResponse(String.format("Билет %s_%s", place.getTripDate().getTrip().getId(), place.getPlaceNumber()), request);
    }

    public void removeOrder(int orderId) {
        dao.removeOrder(orderId);
    }

    public List<OrderResponse> searchOrders(String fromStation,
                                            String toStation,
                                            String busName,
                                            LocalDate fromDate,
                                            LocalDate toDate,
                                            UserType type,
                                            Integer clientId,
                                            String token) {
        List<Order> orders;
        List<OrderResponse> orderResponses = new ArrayList<>();
        if (type.equals(UserType.ADMIN)) {
            orders = dao.searchOrdersForAdmin(fromStation, toStation, busName, fromDate, toDate, clientId);
            orders.forEach(order -> orderResponses.add(createOrderResponse(order)));
            return orderResponses;
        }
        orders = dao.searchOrdersForClient(fromStation, toStation, busName, fromDate, toDate, token);
        orders.forEach(order -> orderResponses.add(createOrderResponse(order)));
        return orderResponses;
    }

    private OrderResponse createOrderResponse(Order order) {
        List<PassengerDTO> passengers = new ArrayList<>();
        order.getPassengers().forEach(passenger -> passengers.add(new PassengerDTO(passenger.getFirstName(), passenger.getLastName(), passenger.getPassportNumber())));
        return new OrderResponse(order.getOrderId(),
                order.getTripDate().getTrip().getId(),
                order.getTripDate().getTrip().getFromStation(),
                order.getTripDate().getTrip().getToStation(),
                order.getTripDate().getTrip().getBus().getBusName(),
                order.getTripDate().getDate().toString(),
                order.getTripDate().getTrip().getStart().toString(),
                order.getTripDate().getTrip().getDuration(),
                order.getTripDate().getTrip().getPrice(),
                order.getTripDate().getTrip().getPrice() * order.getPassengers().size(),
                passengers
        );
    }
}
