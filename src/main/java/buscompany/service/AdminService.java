package buscompany.service;

import buscompany.dao.AdminDao;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.AdminUpdateRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.AdminUpdateInfo;
import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;


public class AdminService {
    private AdminDao dao;
    @Value("${user_idle_timeout}")
    private int userIdleTimeout;

    @Autowired
    public AdminService(AdminDao dao) {
        this.dao = dao;
    }

    public AdminDTO insertAdmin(AdminRegistrationRequest request, String token) throws ServerException {
        Admin admin = dao.insert(new Admin(request.getFirstName(),
                        request.getLastName(),
                        request.getPatronymic(),
                        request.getPosition(),
                        request.getLogin(),
                        request.getPassword(),
                        UserType.ADMIN),
                token,
                LocalDateTime.now().plusSeconds(userIdleTimeout));
        return new AdminDTO(admin.getId(),
                admin.getFirstName(),
                admin.getLastName(),
                admin.getPatronymic(),
                admin.getLogin(),
                admin.getType().name(),
                admin.getPosition());
    }

    public AdminDTO getAdminAccountInfo(String token) {
        Admin admin = dao.getAdmin(token);
        return new AdminDTO(admin.getId(),
                admin.getFirstName(),
                admin.getLastName(),
                admin.getPatronymic(),
                admin.getLogin(),
                admin.getType().name(),
                admin.getPosition());
    }

    public AdminUpdateInfo updateAdmin(Admin admin, AdminUpdateRequest request) {
        admin.setFirstName(request.getFirstName());
        admin.setLastName(request.getLastName());
        admin.setPatronymic(request.getPatronymic());
        admin.setPosition(request.getPosition());
        dao.update(admin, request.getNewPassword());
        return new AdminUpdateInfo(
                request.getFirstName(),
                request.getLastName(),
                request.getPatronymic(),
                request.getPosition(),
                UserType.ADMIN.name()
        );
    }

    public void clearDataBase() {
        dao.clearDataBase();
    }

    public void delete(String token) throws ServerException {
        dao.remove(token);
    }
}
