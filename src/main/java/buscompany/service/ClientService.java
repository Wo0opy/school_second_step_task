package buscompany.service;

import buscompany.dao.ClientDao;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.ClientUpdateRequest;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.ClientUpdateInfo;
import buscompany.exception.ServerException;
import buscompany.model.Client;
import buscompany.model.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;
import java.util.List;

public class ClientService {
    private ClientDao dao;
    @Value("${user_idle_timeout}")
    private int userIdleTimeout;

    @Autowired
    public ClientService(ClientDao dao) {
        this.dao = dao;
    }

    public ClientDTO insert(ClientRegistrationRequest request, String token) throws ServerException {
        Client client = dao.insert(new Client(request.getFirstName(),
                request.getLastName(),
                request.getPatronymic(),
                request.getEmail(),
                request.getPhoneNumber(),
                request.getLogin(),
                request.getPassword(),
                UserType.CLIENT), token, LocalDateTime.now().plusSeconds(userIdleTimeout));
        return new ClientDTO(client.getId(),
                client.getFirstName(),
                client.getLastName(),
                client.getPatronymic(),
                client.getLogin(),
                client.getType().name(),
                client.getEmail(),
                client.getPhoneNumber());
    }

    public ClientDTO getClientAccountInfo(String token) {
        Client client = dao.getClient(token);
        return new ClientDTO(client.getId(),
                client.getFirstName(),
                client.getLastName(),
                client.getPatronymic(),
                client.getLogin(),
                client.getType().name(),
                client.getEmail(),
                client.getPhoneNumber());
    }

    public ClientUpdateInfo updateClient(Client client, ClientUpdateRequest request) {
        client.setFirstName(request.getFirstName());
        client.setLastName(request.getLastName());
        client.setPatronymic(request.getPatronymic());
        client.setEmail(request.getEmail());
        client.setPhoneNumber(request.getPhoneNumber());
        dao.updateClient(client, request.getNewPassword());
        return new ClientUpdateInfo(request);
    }

    public List<Client> getAll() {
        return dao.getAll();
    }

    public void delete(String token) {
        dao.delete(token);
    }
}
