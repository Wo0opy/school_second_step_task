package buscompany.dao;


import buscompany.exception.ServerException;
import buscompany.model.Admin;

import java.time.LocalDateTime;

public interface AdminDao {
    Admin insert(Admin admin, String token, LocalDateTime localDateTime) throws ServerException;

    Admin getAdmin(String token);

    void update(Admin admin, String newPassword);

    void clearDataBase();

    void remove(String token) throws ServerException;
}
