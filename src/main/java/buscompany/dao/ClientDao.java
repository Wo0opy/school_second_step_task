package buscompany.dao;

import buscompany.exception.ServerException;
import buscompany.model.Client;

import java.time.LocalDateTime;
import java.util.List;

public interface ClientDao {
    Client insert(Client client, String token, LocalDateTime localDateTime) throws ServerException;

    List<Client> getAll();

    void updateClient(Client client, String newPassword);

    void delete(String token);

    Client getClient(String token);
}
