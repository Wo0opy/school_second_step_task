package buscompany.dao;

import buscompany.exception.ServerException;
import buscompany.model.Client;
import buscompany.model.Order;
import buscompany.model.TripDate;

import java.time.LocalDate;
import java.util.List;

public interface OrderDao {
    void removeOrder(int orderId);

    List<Integer> getFreePlaces(int tripId);

    Order createOrder(Order order, Client client) throws ServerException;

    TripDate takePlace(String passportNumber, int placeNumber, int orderId);

    List<Order> searchOrdersForAdmin(String fromStation, String toStation, String busName, LocalDate fromDate, LocalDate toDate, Integer clientId);

    List<Order> searchOrdersForClient(String fromStation, String toStation, String busName, LocalDate fromDate, LocalDate toDate, String token);

    TripDate getTripDate(int tripId, LocalDate date);

    int reserveSeats(TripDate tripDate, int placeCount);
}
