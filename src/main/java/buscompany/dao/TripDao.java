package buscompany.dao;

import buscompany.model.Bus;
import buscompany.model.Trip;

import java.time.LocalDate;
import java.util.List;

public interface TripDao {
    Bus getBusByName(String busName);

    void insertTrip(Trip trip);

    void approveTrip(int tripId);

    void removeTrip(int id);

    Trip getTrip(int tripId);

    List<Trip> searchTrips(String fromStation, String toStation, String busName, boolean approved, LocalDate fromDate, LocalDate toDate);

    int updateTrip(Trip trip);

    List<Bus> getBuses();
}
