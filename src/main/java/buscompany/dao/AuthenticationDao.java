package buscompany.dao;

import buscompany.exception.ServerException;
import buscompany.model.Admin;
import buscompany.model.Client;
import buscompany.model.Session;

import java.time.LocalDateTime;

public interface AuthenticationDao {

    String login(String login, String password, LocalDateTime dateTime) throws ServerException;

    void logout(String value);

    void updateSessionDate(String token, LocalDateTime dateTime);

    String getPassword(String token);

    int checkOrderAccess(String token, int orderId);

    Admin getAdminByToken(String token);

    Client getClientByToken(String token);

    Session getSession(String token);
}
