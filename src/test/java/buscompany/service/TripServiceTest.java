package buscompany.service;

import buscompany.dao.TripDao;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.admin.TripUpdateRequest;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.schedule.ScheduleDTO;
import buscompany.exception.ServerException;
import buscompany.model.Bus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TripServiceTest {
    private TripService service;
    @Mock
    private TripDao dao;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        service = new TripService(dao);
    }

    @Test
    public void testInsertTrip_fail() {
        List<LocalDate> dates = new ArrayList<>();
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        try {
            service.insertTrip(tripRegistrationRequest);
            fail();
        } catch (ServerException e) {
            assertEquals("DATES", e.getField());
        }
        tripRegistrationRequest.setSchedule(null);
        tripRegistrationRequest.setDates(null);
        try {
            service.insertTrip(tripRegistrationRequest);
            fail();
        } catch (ServerException e) {
            assertEquals("SCHEDULE/DATES", e.getField());
        }
    }

    @Test
    public void testUpdateTrip_fail() {
        List<LocalDate> dates = new ArrayList<>();
        TripUpdateRequest tripUpdateRequest =
                new TripUpdateRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        try {
            service.updateTrip(tripUpdateRequest, 1);
            fail();
        } catch (ServerException e) {
            assertEquals("DATES", e.getField());
        }
        tripUpdateRequest.setSchedule(null);
        tripUpdateRequest.setDates(null);
        try {
            service.updateTrip(tripUpdateRequest, 1);
            fail();
        } catch (ServerException e) {
            assertEquals("SCHEDULE/DATES", e.getField());
        }
    }

    @Test
    public void testInsertTrip_success() throws ServerException {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        Mockito.when(this.dao.updateTrip(Mockito.any())).thenReturn(1);
        service.insertTrip(tripRegistrationRequest);
        Mockito.verify(this.dao, Mockito.times(1)).insertTrip(Mockito.any());
    }

    @Test
    public void testUpdateTrip_success() throws ServerException {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripUpdateRequest tripUpdateRequest =
                new TripUpdateRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "01:00", 22, null, dates);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        Mockito.when(this.dao.updateTrip(Mockito.any())).thenReturn(1);
        service.updateTrip(tripUpdateRequest, 1);
        Mockito.verify(this.dao, Mockito.times(1)).updateTrip(Mockito.any());
    }

    @Test
    public void testCalculateDates_odd() throws ServerException {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "odd");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripAdminInfo tripAdminInfo = service.insertTrip(tripRegistrationRequest);
        Set<String> expectingSet = new HashSet<>();
        expectingSet.add("2022-09-07");
        expectingSet.add("2022-09-09");
        assertTrue(expectingSet.containsAll(tripAdminInfo.getDates()));
        assertEquals(2, tripAdminInfo.getDates().size());
    }

    @Test
    public void testCalculateDates_even() throws ServerException {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "even");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripAdminInfo tripAdminInfo = service.insertTrip(tripRegistrationRequest);
        Set<String> expectingSet = new HashSet<>();
        expectingSet.add("2022-09-06");
        expectingSet.add("2022-09-08");
        expectingSet.add("2022-09-10");
        assertTrue(expectingSet.containsAll(tripAdminInfo.getDates()));
        assertEquals(3, tripAdminInfo.getDates().size());
    }

    @Test
    public void testCalculateDates_daily() throws ServerException {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "daily");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripAdminInfo tripAdminInfo = service.insertTrip(tripRegistrationRequest);
        Set<String> expectingSet = new HashSet<>();
        expectingSet.add("2022-09-06");
        expectingSet.add("2022-09-07");
        expectingSet.add("2022-09-08");
        expectingSet.add("2022-09-09");
        expectingSet.add("2022-09-10");
        assertTrue(expectingSet.containsAll(tripAdminInfo.getDates()));
        assertEquals(5, tripAdminInfo.getDates().size());
    }

    @Test
    public void testCalculateDates_days_of_week_success() throws ServerException {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "Sat,Tue,Thu");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripAdminInfo tripAdminInfo = service.insertTrip(tripRegistrationRequest);
        Set<String> expectingSet = new HashSet<>();
        expectingSet.add("2022-09-06");
        expectingSet.add("2022-09-08");
        expectingSet.add("2022-09-10");
        assertTrue(expectingSet.containsAll(tripAdminInfo.getDates()));
        assertEquals(3, tripAdminInfo.getDates().size());
    }

    @Test
    public void testCalculateDates_days_of_month() throws ServerException {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "6,9");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        TripAdminInfo tripAdminInfo = service.insertTrip(tripRegistrationRequest);
        Set<String> expectingSet = new HashSet<>();
        expectingSet.add("2022-09-06");
        expectingSet.add("2022-09-09");
        assertTrue(expectingSet.containsAll(tripAdminInfo.getDates()));
        assertEquals(2, tripAdminInfo.getDates().size());
    }

    @Test
    public void testCalculateDates_days_of_week_fail() {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "Sat,Tue,Thus");
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        try {
            service.insertTrip(tripRegistrationRequest);
            fail();
        } catch (ServerException e) {
            assertEquals("SCHEDULE", e.getField());
        }
    }

    @Test
    public void testCalculateDates_no_calculated_dates_fail() {
        ScheduleDTO schedule = new ScheduleDTO("2022-09-06", "2022-09-10", "Mon");
        TripUpdateRequest tripUpdateRequest =
                new TripUpdateRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, schedule, null);
        Mockito.when(this.dao.getBusByName("MAZ")).thenReturn(new Bus("MAZ", 30));
        try {
            service.updateTrip(tripUpdateRequest, 1);
            fail();
        } catch (ServerException e) {
            assertEquals("SCHEDULE", e.getField());
        }
    }

}
