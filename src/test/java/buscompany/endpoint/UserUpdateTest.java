package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.AdminUpdateRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.ClientUpdateRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.error.ErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserUpdateTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void cleanDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void testSuccessUpdateAdmin() {
        AdminUpdateRequest updateRequest = new AdminUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "Администратор", "Pupkin123", "Pupkin333");
        AdminRegistrationRequest request = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "AdminPupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<AdminDTO> response = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        ResponseEntity<AdminDTO> updateResponse = template.exchange("http://localhost:8888/api/admins", HttpMethod.PUT, httpEntity,
                AdminDTO.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());
        assertEquals("Васильевич", updateResponse.getBody().getPatronymic());
    }

    @Test
    public void testSuccessUpdateClient() {
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        ClientUpdateRequest updateRequest = new ClientUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "mymail@mail.ru", "+79999999999", "Pupkin123", "Pupkin1234");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<ClientDTO> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        ResponseEntity<ClientDTO> updateResponse = template.exchange("http://localhost:8888/api/clients", HttpMethod.PUT, httpEntity,
                ClientDTO.class);
        assertEquals(HttpStatus.OK, updateResponse.getStatusCode());
        assertEquals("Васильевич", updateResponse.getBody().getPatronymic());
    }

    @Test
    public void testUpdateClient_fail_by_password() {
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        ClientUpdateRequest updateRequest = new ClientUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "mymail@mail.ru", "+79999999999", "Pupkin1235", "Pupkin1234");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<ClientDTO> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/clients", HttpMethod.PUT, httpEntity,
                    ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void testUpdateAdmin_fail_by_password() {
        AdminUpdateRequest updateRequest = new AdminUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "Администратор", "Pupkin1235", "Pupkin333");
        AdminRegistrationRequest request = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "AdminPupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(request, httpHeaders);
        ResponseEntity<AdminDTO> response = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/admins", HttpMethod.PUT, httpEntity,
                    AdminDTO.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void testUpdateClient_fail_by_userType() {
        ClientUpdateRequest updateRequest = new ClientUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "mymail@mail.ru", "+79999999999", "Pupkin123", "Pupkin1234");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(clientRegistrationRequest, httpHeaders);
        template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "AdminPupkin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> response = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/clients", HttpMethod.PUT, httpEntity,
                    AdminDTO.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void testUpdateAdmin_fail_by_userType() {
        AdminUpdateRequest updateRequest = new AdminUpdateRequest(
                "Василий", "Пупкин", "Васильевич", "Администратор", "Pupkin123", "Pupkin333");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(clientRegistrationRequest, httpHeaders);
        ResponseEntity<ClientDTO> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "AdminPupkin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/admins", HttpMethod.PUT, httpEntity,
                    AdminDTO.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }
}
