package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.passenger.PassengerDTO;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.OrderRegistrationRequest;
import buscompany.dto.request.client.TakePlacesRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.OrderResponse;
import buscompany.dto.response.client.TakePlacesResponse;
import buscompany.dto.response.client.TripClientInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class OrderTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void insertEntities() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:00"), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "PupkinClient", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> infoResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = infoResponseEntity.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        template.postForObject("http://localhost:8888/api/clients", clientRegistrationRequest, ClientDTO.class);
    }

    @Test
    public void orderRegistration_success() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        ResponseEntity<OrderResponse> orderResponse =
                template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
        assertEquals(LocalDate.now().toString(), orderResponse.getBody().getDate());
    }

    @Test
    public void orderTakePlaces_success() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        ResponseEntity<OrderResponse> orderResponse =
                template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
        int orderId = orderResponse.getBody().getOrderId();
        TakePlacesRequest takePlacesRequest = new TakePlacesRequest(orderId, "Василий", "Пупкин", "0000000000", 1);
        httpEntity = new HttpEntity<>(takePlacesRequest, httpHeaders);
        ResponseEntity<TakePlacesResponse> takePlaceResponse =
                template.exchange("http://localhost:8888/api/places", HttpMethod.POST, httpEntity, TakePlacesResponse.class);
        assertEquals(new TakePlacesResponse(String.format("Билет %s_%s", tripId, 1), takePlacesRequest), takePlaceResponse.getBody());
    }

    @Test
    public void removeOrder_success() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        ResponseEntity<OrderResponse> orderResponse =
                template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
        int orderId = orderResponse.getBody().getOrderId();
        ResponseEntity<String> deleteResponse =
                template.exchange("http://localhost:8888/api/orders/" + orderId, HttpMethod.DELETE, httpEntity, String.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
    }

    @Test
    public void removeOrder_fail() {
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/orders/0", HttpMethod.DELETE, httpEntity, String.class);
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }

    @Test
    public void orderRegistration_fail_by_placeCount() {
        List<PassengerDTO> passengers = new ArrayList<>();
        long passportNumber = 1000000000;
        for (int i = 0; i < 31; i++) {
            passportNumber++;
            passengers.add(new PassengerDTO("Василий", "Пупкин", String.valueOf(passportNumber)));
        }
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"NOT_ENOUGH_FREE_PLACES\",\"field\":\"PASSENGERS\",\"message\":\"Недостаточно свободных мест\"}]}", e.getResponseBodyAsString());
        }
    }

    @Test
    public void orderRegistration_fail_wrong_tripDate() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now().plusDays(1), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"WRONG_DATE_OR_ID\",\"field\":\"DATE/ID\",\"message\":\"Неверно указана дата или номер рейса\"}]}", e.getResponseBodyAsString());
        }
    }

    @Test
    public void orderRegistration_fail_wrong_trip_id() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId() + 1;
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"WRONG_DATE_OR_ID\",\"field\":\"DATE/ID\",\"message\":\"Неверно указана дата или номер рейса\"}]}", e.getResponseBodyAsString());
        }
    }
}
