package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.client.ClientDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class UserRegistrationTest {
    private RestTemplate template = new RestTemplate();

    @BeforeEach
    void cleanAndInsert() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        template.postForObject("http://localhost:8888/api/admins", adminRegistrationRequest, AdminDTO.class);
    }

    @Test
    public void testAdminRegistration_success() {
        AdminRegistrationRequest request = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        assertEquals(template.postForObject("http://localhost:8888/api/admins", request, AdminDTO.class).getPosition(),
                "Администратор");
    }

    @Test
    public void testClientRegistration_success() {
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        assertEquals(template.postForObject("http://localhost:8888/api/clients", request, ClientDTO.class).getPhoneNumber(),
                "+79999999999");
    }

    @Test
    public void testAdminRegistration_fail_byDuplicateLogin_after_delete() {
        AdminRegistrationRequest registrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(registrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> response = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> deleteResponse =
                template.exchange("http://localhost:8888/api/accounts", HttpMethod.DELETE, httpEntity, String.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
        httpEntity = new HttpEntity<>(registrationRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                    AdminDTO.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"LOGIN_ALREADY_EXIST\",\"field\":\"login\",\"message\":\"Пользователь с ником Pupkin уже существует\"}]}",
                    e.getResponseBodyAsString());
        }
    }
}
