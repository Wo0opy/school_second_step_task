package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.response.error.ErrorResponse;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AdminRegistrationFailTest {
    private RestTemplate template = new RestTemplate();

    public static Stream<Arguments> nullFieldAdminData() {
        return Stream.of(
                Arguments.arguments(
                        new AdminRegistrationRequest(null, "Пупкин", null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"firstName\",\"message\":\"Укажите имя.\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", null, null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"lastName\",\"message\":\"Укажите фамилию\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, null, "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"position\",\"message\":\"Введите занимаемую должность\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, "Администратор", null, "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"login\",\"message\":\"Введите имя пользователя\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, "Администратор", "Pupkin", null),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"password\",\"message\":\"Введите пароль\"}]}")
        );
    }

    @ParameterizedTest
    @MethodSource("nullFieldAdminData")
    public void testAdminRegistration_nullFieldFail(AdminRegistrationRequest registrationRequest, String expect) {
        try {
            template.postForObject("http://localhost:8888/api/admins", registrationRequest, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(400, e.getStatusCode().value());
            assertEquals(e.getResponseBodyAsString(), expect);
        }
    }

    public static Stream<Arguments> wrongFieldLengthAdminData() {
        return Stream.of(
                Arguments.arguments(
                        new AdminRegistrationRequest("ВасилийВасилийВасилийВасилийВасилийВасилийВасилийВасилий",
                                "Пупкин", null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"firstName\",\"message\":\"Длина имени не более 50ти символов\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася",
                                "ПупкинПупкинПупкинПупкинПупкинПупкинПупкинПупкинПупкин",
                                null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"lastName\",\"message\":\"Длина фамилии не более 50ти символов\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин",
                                "ВасильевичВасильевичВасильевичВасильевичВасильевичВасильевичВасильевич",
                                "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"patronymic\",\"message\":\"Длина отчетсва не более 50ти символов\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, "Администратор",
                                "PupkinPupkinPupkinPupkinPupkinPupkinPupkinPupkinPupkinPupkin",
                                "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"login\",\"message\":\"Длина имени пользователя не более 50ти символов\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, "Администратор", "Pupkin",
                                "Password123Password123Password123Password123Password123Password123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"password\",\"message\":\"Длина пароля от 8ти до 50ти символов\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", null, "Администратор", "Pupkin",
                                "Pass123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"password\",\"message\":\"Длина пароля от 8ти до 50ти символов\"}]}")
        );
    }

    @ParameterizedTest
    @MethodSource("wrongFieldLengthAdminData")
    public void testAdminRegistration_fieldLengthFail(AdminRegistrationRequest registrationRequest, String expect) {
        try {
            template.postForObject("http://localhost:8888/api/admins", registrationRequest, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(400, e.getStatusCode().value());
            assertEquals(e.getResponseBodyAsString(), expect);
        }
    }

    public static Stream<Arguments> forbiddenSymbolsAdminData() {
        return Stream.of(
                Arguments.arguments(
                        new AdminRegistrationRequest("wrong_language",
                                "Пупкин", null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"firstName\",\"message\":\"Имя может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася",
                                "wrong_language",
                                null, "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"lastName\",\"message\":\"Фамилия может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин",
                                "wrong_language",
                                "Администратор", "Pupkin", "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"patronymic\",\"message\":\"Отчество может содержать только русские буквы, пробелы и знак “минус”\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("9", "Пупкин", null, "Администратор",
                                "Pupkin",
                                "Pupkin123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"firstName\",\"message\":\"Имя может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "9", null, "Администратор", "Pupkin",
                                "Password123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"lastName\",\"message\":\"Фамилия может содержать только русские буквы, пробелы и знак “минус” и не может быть пустым\"}]}"),
                Arguments.arguments(
                        new AdminRegistrationRequest("Вася", "Пупкин", "9", "Администратор", "Pupkin",
                                "Password123"),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"patronymic\",\"message\":\"Отчество может содержать только русские буквы, пробелы и знак “минус”\"}]}")
        );
    }

    @ParameterizedTest
    @MethodSource("forbiddenSymbolsAdminData")
    public void testAdminRegistration_forbiddenSymbolsFail(AdminRegistrationRequest registrationRequest, String expect) {
        try {
            template.postForObject("http://localhost:8888/api/admins", registrationRequest, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(400, e.getStatusCode().value());
            assertEquals(e.getResponseBodyAsString(), expect);
        }
    }
}
