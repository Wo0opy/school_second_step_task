package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.TripClientInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetTripsWithParamsTest {
    RestTemplate template = new RestTemplate();

    /**
     * Inserts one Admin (login: PupkinAdmin PWD: Pupkin123 ),
     * one Client  (login: PupkinClient PWD: Pupkin123 ),
     * three trips (and approved one of them)
     */
    @BeforeEach
    void insertEntities() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        List<LocalDate> dates1 = new ArrayList<>();
        dates1.add(LocalDate.now().plusDays(5));
        TripRegistrationRequest tripRegistrationRequest1 =
                new TripRegistrationRequest("PAZ", "Тара", "Омск", LocalTime.parse("22:10"), "22:10", 22, null, dates1);
        List<LocalDate> dates2 = new ArrayList<>();
        dates2.add(LocalDate.now().plusDays(5));
        TripRegistrationRequest tripRegistrationRequest2 =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates2);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> firstTrip = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        httpEntity = new HttpEntity<>(tripRegistrationRequest1, httpHeaders);
        template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        httpEntity = new HttpEntity<>(tripRegistrationRequest2, httpHeaders);
        template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = firstTrip.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "PupkinClient", "Pupkin123");
        template.postForObject("http://localhost:8888/api/clients", request, ClientDTO.class);
    }

    @Test
    public void searchTripsWithParam_admin_request() {
        ParameterizedTypeReference<List<TripAdminInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        httpHeaders.add("Cookie", loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE));
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripAdminInfo>> firstSearchResponse =
                template.exchange("http://localhost:8888/api/trips?busName=MAZ", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        ResponseEntity<List<TripAdminInfo>> secondSearchResponse =
                template.exchange("http://localhost:8888/api/trips?busName=PAZ&fromStation=Тара", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        ResponseEntity<List<TripAdminInfo>> thirdSearchResponse =
                template.exchange(String.format("http://localhost:8888/api/trips?busName=MAZ&fromStation=Омск&toStation=Тара&fromDate=%s&toDate=%s", LocalDate.now().plusDays(2), LocalDate.now().plusDays(5)), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(2, firstSearchResponse.getBody().size());
        assertEquals(1, secondSearchResponse.getBody().size());
        assertEquals("PAZ", secondSearchResponse.getBody().get(0).getBusName());
        assertEquals(1, thirdSearchResponse.getBody().size());
    }

    @Test
    public void searchTripsWithParam_client_request() {
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        httpHeaders.add("Cookie", loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE));
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> firstSearchResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        ResponseEntity<List<TripClientInfo>> secondSearchResponse =
                template.exchange("http://localhost:8888/api/trips?busName=PAZ", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        ResponseEntity<List<TripClientInfo>> thirdSearchResponse =
                template.exchange(String.format("http://localhost:8888/api/trips?fromDate=%s&toDate=%s", LocalDate.now().plusDays(2), LocalDate.now().plusDays(5)), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        ResponseEntity<List<TripClientInfo>> fourthSearchResponse =
                template.exchange(String.format("http://localhost:8888/api/trips?fromDate=%s", LocalDate.now()), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(1, firstSearchResponse.getBody().size());
        assertEquals(0, secondSearchResponse.getBody().size());
        assertEquals(0, thirdSearchResponse.getBody().size());
        assertEquals(1, fourthSearchResponse.getBody().size());
    }
}
