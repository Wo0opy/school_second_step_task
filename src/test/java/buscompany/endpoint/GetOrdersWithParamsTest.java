package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.passenger.PassengerDTO;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.OrderRegistrationRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.OrderResponse;
import buscompany.dto.response.client.TripClientInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetOrdersWithParamsTest {
    RestTemplate template = new RestTemplate();

    /**
     * Inserts one Admin (login: PupkinAdmin PWD: Pupkin123 ),
     * one Client  (login: PupkinClient PWD: Pupkin123 ),
     * one trip (and approved it)
     * and one order.
     */
    @BeforeEach
    void insertEntities() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> firstTrip = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = firstTrip.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "PupkinClient", "Pupkin123");
        template.postForObject("http://localhost:8888/api/clients", request, ClientDTO.class);
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
    }

    @Test
    public void getOrdersByAdmin_by_toDate() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toDate=" + LocalDate.now(), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toDate=" + LocalDate.now().minusDays(1), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByAdmin_by_fromDate() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromDate=" + LocalDate.now(), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromDate=" + LocalDate.now().plusDays(1), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByAdmin_by_fromStation() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromStation=Омск", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromStation=Тара", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByAdmin_by_toStation() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toStation=Тара", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toStation=Омск", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByClient_by_fromDate() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromDate=" + LocalDate.now(), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromDate=" + LocalDate.now().plusDays(1), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByClient_by_toDate() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toDate" + LocalDate.now(), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toDate=" + LocalDate.now().minusDays(1), HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByClient_by_fromStation() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromStation=Омск", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?fromStation=Тара", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }

    @Test
    public void getOrdersByClient_by_toStation() {
        ParameterizedTypeReference<List<OrderResponse>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<OrderResponse>> searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toStation=Тара", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(1, searchOrderResponse.getBody().size());
        assertEquals(new PassengerDTO("Василий", "Пупкин", "0000000000"), searchOrderResponse.getBody().get(0).getPassengers().get(0));
        searchOrderResponse =
                template.exchange("http://localhost:8888/api/orders/?toStation=Омск", HttpMethod.GET, httpEntity, parameterizedTypeReference);
        assertEquals(HttpStatus.OK, searchOrderResponse.getStatusCode());
        assertEquals(0, searchOrderResponse.getBody().size());
    }
}
