package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.bus.BusDTO;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetBusesTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void getBusesTest() {
        List<BusDTO> buses = new ArrayList<>();
        buses.add(new BusDTO("ZIZ", 20));
        buses.add(new BusDTO("MAZ", 30));
        buses.add(new BusDTO("PAZ", 40));
        ParameterizedTypeReference<List<BusDTO>> parameterizedTypeReference = new ParameterizedTypeReference<>() {
        };
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Admin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> accountInfoResponseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = accountInfoResponseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<BusDTO>> response = template.exchange("http://localhost:8888/api/buses", HttpMethod.GET, httpEntity,
                parameterizedTypeReference);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, response.getBody().size());
        assertTrue(response.getBody().containsAll(buses));
    }
}
