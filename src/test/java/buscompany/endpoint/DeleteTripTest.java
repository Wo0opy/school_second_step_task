package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.tripinfo.TripInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DeleteTripTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    void deleteTrip_success() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.now(), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = tripResponseEntity.getBody().getId();
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.DELETE, httpEntity, (Class<Object>) null);
        ResponseEntity<TripInfo> tripInfoResponseEntity = template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.GET, httpEntity, TripInfo.class);
        assertEquals(HttpStatus.OK, tripInfoResponseEntity.getStatusCode());
        assertNull(tripInfoResponseEntity.getBody());
    }
}
