package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.error.ErrorResponse;
import buscompany.dto.schedule.ScheduleDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TripRegistrationTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void tripRegistration_success() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        dates.add(LocalDate.now().plusDays(1));
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        assertEquals(HttpStatus.OK, tripResponseEntity.getStatusCode());
        assertFalse(tripResponseEntity.getBody().isApproved());
        int id = tripResponseEntity.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<TripAdminInfo> approveTripResponseEntity = template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        assertEquals(HttpStatus.OK, approveTripResponseEntity.getStatusCode());
        assertTrue(approveTripResponseEntity.getBody().isApproved());
    }

    public static Stream<Arguments> incorrectFieldTripData() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        String type = "EVEN";
        if (((LocalDate.now().getDayOfMonth()) % 2) == 0) {
            type = "ODD";
        }
        return Stream.of(
                Arguments.arguments(
                        new TripRegistrationRequest("", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"busName\",\"message\":\"Выберете марку автобуса\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"fromStation\",\"message\":\"Укажите станцию отправления\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "", LocalTime.parse("22:10"), "22:10", 22, null, dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"toStation\",\"message\":\"Укажите станцию прибытия\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "", 22, null, dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"duration\",\"message\":\"Укажите время в формате ЧЧ:ММ\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 0, null, dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_PARAMETER\",\"field\":\"price\",\"message\":\"Укажите цену билета\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, new ScheduleDTO(LocalDate.now().toString(), LocalDate.now().toString(), type), null),
                        String.format("{\"errors\":[{\"errorCode\":\"NO_TRIP_DATES_CALCULATED\",\"field\":\"SCHEDULE\",\"message\":\"Нет дат соответствующих расписанию рейса: ScheduleDTO(fromDate=%s, toDate=%s, period=%s)\"}]}", LocalDate.now(), LocalDate.now(), type)),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, null),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_SCHEDULE_OR_DATES\",\"field\":\"SCHEDULE/DATES\",\"message\":\"В запросе необходимо указать один из параметров - или расписание или даты отправления.\"}]}"),
                Arguments.arguments(
                        new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, new ScheduleDTO(LocalDate.now().toString(), LocalDate.now().plusDays(1).toString(), "ODD"), dates),
                        "{\"errors\":[{\"errorCode\":\"INCORRECT_SCHEDULE_OR_DATES\",\"field\":\"SCHEDULE/DATES\",\"message\":\"В запросе необходимо указать один из параметров - или расписание или даты отправления.\"}]}")
        );
    }

    @ParameterizedTest
    @MethodSource("incorrectFieldTripData")
    public void tripRegistration_fail(TripRegistrationRequest tripRegistrationRequest, String expect) {
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
            assertEquals(expect, e.getResponseBodyAsString());
        }

    }
}
