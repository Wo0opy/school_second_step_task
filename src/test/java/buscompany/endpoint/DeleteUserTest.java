package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.client.ClientDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DeleteUserTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearAndInsert() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        template.postForObject("http://localhost:8888/api/admins", adminRegistrationRequest, AdminDTO.class);
        ClientRegistrationRequest request = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "PupkinClient", "Pupkin123");
        template.postForObject("http://localhost:8888/api/clients", request, ClientDTO.class);
    }

    @Test
    public void deleteClient_success() {
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> deleteResponse =
                template.exchange("http://localhost:8888/api/accounts", HttpMethod.DELETE, httpEntity, String.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
        httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"INVALID_LOGIN_OR_PASSWORD\",\"field\":\"LOGIN/PASSWORD\",\"message\":\"Неправильный логин или пароль\"}]}",
                    e.getResponseBodyAsString());
        }
    }

    @Test
    public void deleteAdmin_success() {
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin2", "Pupkin123");
        template.postForObject("http://localhost:8888/api/admins", adminRegistrationRequest, AdminDTO.class);
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> deleteResponse =
                template.exchange("http://localhost:8888/api/accounts", HttpMethod.DELETE, httpEntity, String.class);
        assertEquals(HttpStatus.OK, deleteResponse.getStatusCode());
        httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"INVALID_LOGIN_OR_PASSWORD\",\"field\":\"LOGIN/PASSWORD\",\"message\":\"Неправильный логин или пароль\"}]}",
                    e.getResponseBodyAsString());
        }
    }

    @Test
    public void deleteClient_fail() {
        LoginRequest loginRequest = new LoginRequest("PupkinAdmin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/accounts", HttpMethod.DELETE, httpEntity, String.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals("{\"errors\":[{\"errorCode\":\"LAST_ADMIN\",\"field\":\"ADMIN\",\"message\":\"Удаление аккаунта вожможно только после регистрации нового администратора\"}]}",
                    e.getResponseBodyAsString());
        }
    }
}
