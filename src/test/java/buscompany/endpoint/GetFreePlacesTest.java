package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.passenger.PassengerDTO;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.client.OrderRegistrationRequest;
import buscompany.dto.request.client.TakePlacesRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.OrderResponse;
import buscompany.dto.response.client.TakePlacesResponse;
import buscompany.dto.response.client.TripClientInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetFreePlacesTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void insertEntities() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "PupkinAdmin", "Pupkin123");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "PupkinClient", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> infoResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = infoResponseEntity.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        template.postForObject("http://localhost:8888/api/clients", clientRegistrationRequest, ClientDTO.class);
    }

    @Test
    public void getFreePlaces() {
        List<PassengerDTO> passengers = new ArrayList<>();
        passengers.add(new
                PassengerDTO("Василий", "Пупкин", "0000000000"));
        ParameterizedTypeReference<List<TripClientInfo>> typeReference = new ParameterizedTypeReference<>() {
        };
        ParameterizedTypeReference<List<Integer>> placesReference = new ParameterizedTypeReference<>() {
        };
        LoginRequest loginRequest = new LoginRequest("PupkinClient", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<String> loginResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity, String.class);
        String token = loginResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<TripClientInfo>> tripResponse =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.GET, httpEntity, typeReference);
        int tripId = tripResponse.getBody().get(0).getId();
        OrderRegistrationRequest orderRegistrationRequest = new OrderRegistrationRequest(tripId, LocalDate.now(), passengers);
        httpEntity = new HttpEntity<>(orderRegistrationRequest, httpHeaders);
        ResponseEntity<OrderResponse> orderResponse =
                template.exchange("http://localhost:8888/api/orders", HttpMethod.POST, httpEntity, OrderResponse.class);
        int orderId = orderResponse.getBody().getOrderId();
        ResponseEntity<List<Integer>> freePlacesResponse =
                template.exchange("http://localhost:8888/api/places/" + orderId, HttpMethod.GET, httpEntity, placesReference);
        assertEquals(30, freePlacesResponse.getBody().size());
        TakePlacesRequest takePlacesRequest = new TakePlacesRequest(orderId, "Василий", "Пупкин", "0000000000", 1);
        httpEntity = new HttpEntity<>(takePlacesRequest, httpHeaders);
        template.exchange("http://localhost:8888/api/places", HttpMethod.POST, httpEntity, TakePlacesResponse.class);
        freePlacesResponse = template.exchange("http://localhost:8888/api/places/" + orderId, HttpMethod.GET, httpEntity, placesReference);
        assertEquals(29, freePlacesResponse.getBody().size());
    }
}
