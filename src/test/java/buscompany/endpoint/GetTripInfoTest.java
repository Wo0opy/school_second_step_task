package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.client.TripClientInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetTripInfoTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void getAdminTripInfo_success() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripRegistrationResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = tripRegistrationResponseEntity.getBody().getId();
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<TripAdminInfo> tripInfoResponseEntity = template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.GET, httpEntity, TripAdminInfo.class);
        assertEquals(HttpStatus.OK, tripInfoResponseEntity.getStatusCode());
        assertEquals(tripInfoResponseEntity.getBody(), tripRegistrationResponseEntity.getBody());
    }

    @Test
    public void getClientTripInfo_success() {
        List<LocalDate> dates = new ArrayList<>();
        List<LocalDate> dates1 = new ArrayList<>();
        dates.add(LocalDate.now());
        dates1.add(LocalDate.now());
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.parse("22:10"), "01:00", 22, null, dates1);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Admin", "Pupkin123");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> adminAccountInfoResponseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = adminAccountInfoResponseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripRegistrationResponseEntity = template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = tripRegistrationResponseEntity.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(clientRegistrationRequest, httpHeaders);
        ResponseEntity<ClientDTO> clientAccountInfoResponseEntity = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        token = clientAccountInfoResponseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<TripClientInfo> tripInfoResponseEntity = template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.GET, httpEntity, TripClientInfo.class);
        assertEquals(HttpStatus.OK, tripInfoResponseEntity.getStatusCode());
        List<String> dateStrings = new ArrayList<>();
        dates.forEach(date -> dateStrings.add(date.toString()));
        assertEquals(tripInfoResponseEntity.getBody(), new TripClientInfo(id, "Омск", "Тара", "MAZ", "22:10", 60, null, 22.0, dateStrings));
    }
}
