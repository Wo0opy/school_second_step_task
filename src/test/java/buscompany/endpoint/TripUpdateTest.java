package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.admin.TripRegistrationRequest;
import buscompany.dto.request.admin.TripUpdateRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.admin.TripAdminInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TripUpdateTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void TripUpdateTest_success() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripUpdateRequest updateRequest =
                new TripUpdateRequest("MAZ", "Тара", "Барабинск", LocalTime.now(), "22:10", 33, null, dates);
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.now(), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripResponseEntity =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = tripResponseEntity.getBody().getId();
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> updateResponseEntity =
                template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        assertEquals(HttpStatus.OK, updateResponseEntity.getStatusCode());
        assertEquals(updateRequest.getFromStation() + updateRequest.getToStation(),
                updateResponseEntity.getBody().getFromStation() + updateResponseEntity.getBody().getToStation());
        assertFalse(updateResponseEntity.getBody().isApproved());
    }

    @Test
    public void TripUpdateTest_fail() {
        List<LocalDate> dates = new ArrayList<>();
        dates.add(LocalDate.now());
        TripUpdateRequest updateRequest =
                new TripUpdateRequest("MAZ", "Тара", "Барабинск", LocalTime.now(), "22:10", 33, null, dates);
        TripRegistrationRequest tripRegistrationRequest =
                new TripRegistrationRequest("MAZ", "Омск", "Тара", LocalTime.now(), "22:10", 22, null, dates);
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> responseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = responseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(tripRegistrationRequest, httpHeaders);
        ResponseEntity<TripAdminInfo> tripResponseEntity =
                template.exchange("http://localhost:8888/api/trips", HttpMethod.POST, httpEntity, TripAdminInfo.class);
        int id = tripResponseEntity.getBody().getId();
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<TripAdminInfo> approveTripResponseEntity =
                template.exchange("http://localhost:8888/api/trips/" + id + "/approve", HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        assertEquals(HttpStatus.OK, approveTripResponseEntity.getStatusCode());
        assertTrue(approveTripResponseEntity.getBody().isApproved());
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(updateRequest, httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/trips/" + id, HttpMethod.PUT, httpEntity, TripAdminInfo.class);
        } catch (HttpClientErrorException e) {
            assertEquals(400, e.getStatusCode().value());
            assertEquals(e.getResponseBodyAsString(), "{\"errors\":[{\"errorCode\":\"UPDATE_FAIL\",\"field\":\"ID\",\"message\":\"Рейс №" + id + " устверждён или не существует\"}]}");
        }
    }
}
