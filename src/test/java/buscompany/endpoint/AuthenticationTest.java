package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.request.login.LoginRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.client.ClientDTO;
import buscompany.dto.response.error.ErrorResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AuthenticationTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void testAdminAuthentication() {
        AdminRegistrationRequest registrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(registrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> response = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/sessions", HttpMethod.DELETE, httpEntity, String.class);
        try {
            template.exchange("http://localhost:8888/api/sessions", HttpMethod.GET, httpEntity, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
        LoginRequest loginRequest = new LoginRequest("Pupkin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<?> logInResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity,
                String.class);
        token = logInResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        assertEquals(HttpStatus.OK, logInResponse.getStatusCode());
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<AdminDTO> admin = template.exchange("http://localhost:8888/api/sessions", HttpMethod.GET, httpEntity, AdminDTO.class);
        assertEquals(HttpStatus.OK, admin.getStatusCode());
        assertEquals(registrationRequest.getFirstName(), admin.getBody().getFirstName());
    }

    @Test
    public void testClientAuthentication() {
        ClientRegistrationRequest registrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(registrationRequest, httpHeaders);
        ResponseEntity<ClientDTO> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/sessions", HttpMethod.DELETE, httpEntity, String.class);
        try {
            template.exchange("http://localhost:8888/api/sessions", HttpMethod.GET, httpEntity, ErrorResponse.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
        LoginRequest loginRequest = new LoginRequest("Pupkin", "Pupkin123");
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpEntity = new HttpEntity<>(loginRequest, httpHeaders);
        ResponseEntity<?> logInResponse = template.exchange("http://localhost:8888/api/sessions", HttpMethod.POST, httpEntity,
                String.class);
        token = logInResponse.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        assertEquals(HttpStatus.OK, logInResponse.getStatusCode());
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ClientDTO> client = template.exchange("http://localhost:8888/api/sessions", HttpMethod.GET, httpEntity, ClientDTO.class);
        assertEquals(HttpStatus.OK, client.getStatusCode());
        assertEquals(registrationRequest.getEmail(), client.getBody().getEmail());
    }

    @Disabled
    @Test
    public void testAuthenticationTimeOut() throws InterruptedException {
        ClientRegistrationRequest registrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(registrationRequest, httpHeaders);
        ResponseEntity<ClientDTO> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.POST, httpEntity,
                ClientDTO.class);
        ResponseEntity<Integer> timeout = template.exchange("http://localhost:8888/api/timeout", HttpMethod.GET, httpEntity,
                Integer.class);
        String token = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        Thread.sleep(timeout.getBody() * 1111);
        httpHeaders = new HttpHeaders();
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        try {
            template.exchange("http://localhost:8888/api/sessions", HttpMethod.GET, httpEntity, ClientDTO.class);
            fail();
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }
}
