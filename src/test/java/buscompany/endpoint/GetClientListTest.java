package buscompany.endpoint;

import buscompany.application.BusCompanyApplication;
import buscompany.dto.request.admin.AdminRegistrationRequest;
import buscompany.dto.request.client.ClientRegistrationRequest;
import buscompany.dto.response.admin.AdminDTO;
import buscompany.dto.response.client.ClientDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BusCompanyApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class GetClientListTest {
    RestTemplate template = new RestTemplate();

    @BeforeEach
    void clearDataBase() {
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        template.exchange("http://localhost:8888/api/debug/clear", HttpMethod.POST, httpEntity, String.class);
    }

    @Test
    public void getClientListTest() {
        ParameterizedTypeReference<List<ClientDTO>> parameterizedTypeReference = new ParameterizedTypeReference<List<ClientDTO>>() {
        };
        AdminRegistrationRequest adminRegistrationRequest = new AdminRegistrationRequest(
                "Василий", "Пупкин", null, "Администратор", "Admin", "Pupkin123");
        ClientRegistrationRequest clientRegistrationRequest = new ClientRegistrationRequest(
                "Василий", "Пупкин", null, "mymail@mail.ru", "+79999999999", "Pupkin", "Pupkin123");
        template.postForObject("http://localhost:8888/api/clients", clientRegistrationRequest, ClientDTO.class);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> httpEntity = new HttpEntity<>(adminRegistrationRequest, httpHeaders);
        ResponseEntity<AdminDTO> accountInfoResponseEntity = template.exchange("http://localhost:8888/api/admins", HttpMethod.POST, httpEntity,
                AdminDTO.class);
        String token = accountInfoResponseEntity.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
        httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        httpHeaders.add("Cookie", token);
        httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<List<ClientDTO>> response = template.exchange("http://localhost:8888/api/clients", HttpMethod.GET, httpEntity,
                parameterizedTypeReference);
        assertEquals(1, response.getBody().size());
        assertEquals(clientRegistrationRequest.getEmail(), response.getBody().get(0).getEmail());
    }
}

